# CEG
Training dataset and examples

The file **res.csv** contains results and instructions on how to obtain the individual images from the example folder<br>
e.g. line 2 [0,s1e5,92,43,c,1,94,100,0] ; <br>this relates to the image number 0 (image00.bmp), the options for which were sharp=True,erode=5 (5 iterations of erosion) with the x and y coordinate being 92,43. The image is in color 'c' (not greyscale) and was obtained at depth 1 (after the first segmentation). The values 'seg', 'ceg', 'orig' correspond to the transcription percentages for the segmented, CEG and extracted (bounding_0.bmp) images after 10 rounds of segmentation (from different areas of the image) selecting best results. The options used were `tesseract {image_in_tif_format} - -l jpn --psm {1,5,7,13} -c lstm_choice_mode=2` when it comes to 'seg' and 'orig', while 'ceg' only used 7 and 13 (single line options). The percentages both include the amount of correctly transcribed text as well as the confidence levels, rounded to the closest 10x (92 => 100, 43 => 50). This only applies to 'orig' and 'seg', as 'ceg' is always presumed to have confidence 0 or 100 for every character. <br>
If a character was given a higer confidence rating, but was actually incorrect, this was marked as if it was 0.<br>
The evaluation formula being:<br>
(1/len(text))\*(100\*N<sub>0</sub>+90\*N<sub>1</sub>...0\*N<sub>10</sub>)<br>
with the final result being the ceiling of the average of all 10 results.<br>
Any excess characters, when it came to 'orig' and 'seg', were discarded.<br><br>
All image data herein is copyright of their respective owners and is only presented for educational purposes.
