import cv2
import numpy as np
import matplotlib.pyplot as plt

def drawCEGimage(imgsize, elm, contours, hierarchy, charspacing=10, padding=30, draw=False):
    flow = 'horizontal'
    widths = []
    heights = []
    for e in elm:
        if e[-1]:
            flow = 'vertical'
        widths.append(e[1] - e[0])
        heights.append(e[3] - e[2])

    #width = np.mean(np.array(widths))

    boxes = []

    for i in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[i])
        a = cv2.contourArea(contours[i])
        boxes.append((x, y, w, h, a, i))
    arranged = boxes
    arranged = sorted(arranged, key=lambda x: x[0] * x[1])

    if len(elm) < 2:
        return np.zeros(imgsize)
    #probable_spacing = elm[1][0] - elm[0][1]
    #probable_width = elm[0][1] - elm[0][0]
    #probable_height = elm[0][3] - elm[0][2]
    probable_width = max(widths)
    probable_height = max(heights)

    size = max(probable_height, probable_width)
    if flow == 'vertical':
        # switch the values around because we are rotating
        full_image = np.zeros((size, size * len(elm) + (len(elm) - 1) * charspacing), dtype=np.uint8)
        for idx, e in enumerate(elm):
            draw_on = np.zeros((imgsize[0], imgsize[1] + charspacing * len(elm)))
            min_x = imgsize[1]
            min_y = imgsize[0]
            max_x = 0
            max_y = 0
            for x, y, w, h, _, i in arranged:
                if e[0] < x + (w // 2) < e[1] and e[2] < y + (h // 2) < e[3]:
                    if hierarchy[0][i][3] == -1:
                        #offset = np.array((x, y)) * -1
                        cv2.drawContours(draw_on, contours, i, 255, -1, hierarchy=hierarchy, maxLevel=2)#,offset=tuple(offset))
                        min_x = min(min_x, x)
                        min_y = min(min_y, y)
                        max_x = max(max_x, x + w)
                        max_y = max(max_y, y + h)

            center = np.array((min_x + (max_x - min_x) // 2, min_y + (max_y - min_y) // 2))
            real_center = np.array((size // 2, size // 2))
            move = real_center - center
            T = np.float32([[1, 0, move[0]], [0, 1, move[1]]])
            empty = cv2.warpAffine(draw_on, T, (size, size))
            #plt.title((max_y-min_y)/(max_x-min_x))
            #plt.imshow(empty)
            #plt.show()
            if (max_y - min_y) / (max_x - min_x) < 0.25:
                print('INDEX', idx, 'CHOUON?')
                M = cv2.getRotationMatrix2D(tuple(real_center), 0, 1)
                empty = cv2.warpAffine(empty, M, (size, size))
            else:
                M = cv2.getRotationMatrix2D(tuple(real_center), -90, 1)
                empty = cv2.warpAffine(empty, M, (size, size))
                elm[idx] = (elm[idx][0], elm[idx][1], elm[idx][2], elm[idx][3], True)
            empty = np.uint8(empty)
            full_image[:, (size + charspacing) * idx:(size + charspacing) * idx + size] = empty

        #plt.figure(figsize=(15,9))
        #plt.title("Rotated -90°")
        #plt.imshow(full_image,cmap='gray')
        #plt.show()
        blank = full_image
        #spacing = probable_spacing
    else:
        blank = np.zeros(imgsize)
        blank = np.uint8(blank)

        full_image = np.zeros((size, size * len(elm) + (len(elm) - 1) * charspacing), dtype=np.uint8)
        for idx, e in enumerate(elm):
            draw_on = np.zeros((imgsize[0], imgsize[1] + charspacing * len(elm)))
            min_x = imgsize[1]
            min_y = e[2]
            max_x = 0
            max_y = e[3]
            for x, y, w, h, _, i in arranged:
                if e[0] < x + (w // 2) < e[1] and e[2] < y + (h // 2) < e[3]:
                    if hierarchy[0][i][3] == -1:
                        cv2.drawContours(draw_on, contours, i, 255, -1, hierarchy=hierarchy, maxLevel=2)#,offset=tuple(offset))
                        min_x = min(min_x, x)
                        #min_y = min(min_y,y)
                        max_x = max(max_x, x + w)
                        #max_y = max(max_y,y+h)

            #plt.imshow(draw_on)
            #plt.show()

            center = np.array((min_x + (max_x - min_x) // 2, min_y + (max_y - min_y) // 2))
            real_center = np.array((size // 2, size // 2))
            move = real_center - center
            T = np.float32([[1, 0, move[0]], [0, 1, move[1]]])
            empty = cv2.warpAffine(draw_on, T, (size, size))
            #plt.title((max_y-min_y)/(max_x-min_x))
            #plt.imshow(empty)
            #plt.show()
            empty = np.uint8(empty)
            full_image[:, (size + charspacing) * idx:(size + charspacing) * idx + size] = empty
        #plt.imshow(full_image)
        #plt.show()
    blank = full_image
    start = 0
    end = (size + charspacing) * idx + size
    blank = blank[:, start:end]

    # RESIZE
    scale = 50 / blank.shape[0]
    new_width = blank.shape[1] * scale
    blank = cv2.resize(blank, (int(new_width), 50))
    #plt.figure(figsize=(50,20))
    #plt.subplot(141)
    #plt.axis('off')
    #plt.imshow(blank,cmap='gray')
    #plt.show()

    padded = np.zeros(np.array(blank.shape) + 2 * padding, dtype=np.uint8)
    padded[padding:padding + blank.shape[0], padding:padding + blank.shape[1]] = blank
    #print(padded.dtype)
    padded = np.bitwise_not(padded)
    if draw:
        plt.figure(figsize=(15, 9))
        plt.axis('off')
        plt.imshow(padded, cmap='gray')
        plt.show()
    return padded


def returnCEGindex(constraints, select):
    if not select or not constraints:
        return -1
    for i, e in enumerate(constraints):
        if e[0] <= select[0] <= e[0] + e[2] and e[1] <= select[1] <= e[1] + e[3]:
            return i
    return -1


def generateSingleChar(index, imagesize, elm, contours, hierarchy, draw=False):
    boxes = []
    for i in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[i])
        a = cv2.contourArea(contours[i])
        boxes.append((x, y, w, h, a, i))
    arranged = boxes
    arranged = sorted(arranged, key=lambda x: x[0] * x[1])

    # these are specified in the OCR training file
    fontsize = 64
    full_size = fontsize + fontsize // 2
    midpoint = full_size // 2

    #empty = np.zeros((full_size,full_size))

    draw_on = np.zeros((imagesize[0], imagesize[1]))

    e = elm[index]
    min_x = imagesize[1]
    min_y = imagesize[0]
    max_x = 0
    max_y = 0
    for x, y, w, h, _, i in arranged:
        if e[0] < x + (w // 2) < e[1] and e[2] < y + (h // 2) < e[3]:
            if hierarchy[0][i][3] == -1:
                #offset = np.array((x, y)) * -1
                cv2.drawContours(draw_on, contours, i, 255, -1, hierarchy=hierarchy, maxLevel=2)#,offset=tuple(offset))
                min_x = min(min_x, x)
                min_y = min(min_y, y)
                max_x = max(max_x, x + w)
                max_y = max(max_y, y + h)
    #plt.imshow(draw_on,cmap='gray')
    #plt.show()
    center = np.array((min_x + (max_x - min_x) // 2, min_y + (max_y - min_y) // 2))
    real_center = np.array((full_size // 2, full_size // 2))
    move = real_center - center
    T = np.float32([[1, 0, move[0]], [0, 1, move[1]]])
    empty = cv2.warpAffine(draw_on, T, (full_size, full_size))

    empty = np.uint8(empty)

    empty = cv2.warpAffine(draw_on, T, (full_size, full_size))

    if e[4]: # rotate
        M = cv2.getRotationMatrix2D(tuple(real_center), -90, 1)
        print(M)
        empty = cv2.warpAffine(empty, M, (full_size, full_size))
        empty = np.uint8(empty)

    #empty = cv2.cvtColor(empty,cv2.COLOR_GRAY2RGB)
    current_size = sum((max_x - min_x, max_y - min_y)) // 2
    #print(current_size)
    scale = 64 // current_size
    if scale == 0:
        scale = np.ceil(current_size / 64)
        print(f"Downscale {scale}x")
        blank = cv2.resize(empty, tuple(np.uint8(np.array(empty.shape, dtype=np.float16) / scale)), interpolation=cv2.INTER_NEAREST)

    else:
        print(f"Rescale {scale}x")
        blank = cv2.resize(empty, tuple(np.array(empty.shape) * scale), interpolation=cv2.INTER_NEAREST)
    #print(blank.shape)
    #print((full_size,full_size))

    axis = blank.shape[0] // 2
    blank = blank[axis - midpoint:axis + midpoint, axis - midpoint:axis + midpoint]
    #print(blank.shape)
    empty = blank
    empty = np.uint8(empty)
    empty = cv2.cvtColor(empty, cv2.COLOR_GRAY2RGB)
    print(empty.shape)

    if draw:
        plt.imshow(empty, cmap='gray')
        plt.show()
    return empty
