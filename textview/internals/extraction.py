from .constants import State, Flow, Color, Direction, PossibleTypes, LOW_RATIO, HIGH_RATIO, MIN_SIZE
import cv2
import numpy as np
import matplotlib.pyplot as plt
import multiprocessing
import uuid
from .transcription import transcribe, Aggregator
from .relations import box_relation, merge, return_center, return_box, proximity_merge, box_area, box_point_test
from .segmentation import segment

sequential = False
preliminary = True
maxdepth = 5


def set_maxdepth(depth):
    global maxdepth
    maxdepth = depth


def sequential_execution(state):
    global sequential
    sequential = state


def preliminary_extraction(state):
    global preliminary
    preliminary = state


def character_discovery(A, elements, shape):
    elements = sorted(elements, key=lambda x: box_relation(A, (x[0], x[1], x[2], x[3]))[0])
    elements = [x for x in elements if x[1] != 0 and x[1] + x[3] != shape[0] and x[0] != 0 and x[0] + x[2] != shape[1]]
    probable = []#[(A,0)]

    tries = 0
    MAX_TRIES = 15
    SCALE = 3

    (x, y, w, h) = A
    radius_of_observation = (max(w, h) // 2) * SCALE

    c, D = merge(A, elements)
    if c == 0:
        if LOW_RATIO <= A[3] / A[2] <= HIGH_RATIO:
            probable.append((A, PossibleTypes.INITIAL_WITH_GOOD_RATIO))
        center = return_center(A)
        bigger = max(A[2], A[3])
        Q = return_box(bigger, center)
        c, E = merge(Q, elements)
        #if c == 1: # only A intersects E
        if E == Q: # there are smaller segments in A
            probable.append((E, PossibleTypes.INITIAL_NO_OVERLAP))

    for x2, y2, w2, h2, ii, e2 in elements:

        if tries > MAX_TRIES:
            break

        B = (x2, y2, w2, h2)
        if A == B:
            continue

        # FIXME temporary fix
        #id = uuid.uuid4().hex
        #print('FIXME')
        #LOCAL_PERFECT = 1.0
        #d, P = box_relation(A, B)
        #print(id, 'd', d)
        #print(id, 'P', P)
        #print(id, 'B', B)
        #print(id, abs(A[3] / A[2] - LOCAL_PERFECT))
        #print(id, abs(B[3] / B[2] - LOCAL_PERFECT))
        #print(id, 'A', A[3] * A[2])
        #print(id, 'B', B[3] * B[2])
        #if d == 0 and P == B and B[2] * B[3] > A[2] * A[3] * 5 and abs(B[3] / B[2] - LOCAL_PERFECT) > abs(A[3] / A[2] - LOCAL_PERFECT):
        #    print(id, 'QUIT')
        #    continue
        #print()

        if A[3] * A[2] < B[3] * B[2]:
            center = return_center(B)
            bigger = max(B[2], B[3])
            Q = return_box(bigger, center)
            c, E = merge(Q, elements)
            if E == Q:
                probable.append((E, PossibleTypes.SQUARED_NO_OVERLAP))
            elif LOW_RATIO <= B[3] / B[2] <= HIGH_RATIO:
                c, E = merge(B, elements)
                if E == B:
                    probable.append((B, PossibleTypes.NO_OVERLAP))
                else:
                    probable.append((B, PossibleTypes.GOOD_RATIO))

        dist, _ = box_relation(A, B)
        if dist < radius_of_observation:
            GG = []
            for i in range(3, -1, -1): # from 3 to 0
                GG.append(proximity_merge(A, elements, dist, skip=i))

            LD = []
            C = GG[0]
            for E in GG:
                if C[1] == E[1]:
                    if E[0] > C[0]:
                        C = E
                else:
                    LD.append(C)
                    C = E
            LD.append(C)
            GG = LD

            for c, G in GG:
                center = return_center(G)
                bigger = max(G[2], G[3])
                Q = return_box(bigger, center)
                num, E = merge(Q, elements)
                if E == Q:
                    probable.append((E, PossibleTypes.SQUARED_PROXIMITY_NO_OVERLAP))
                elif LOW_RATIO <= G[3] / G[2] <= HIGH_RATIO:
                    num, E = merge(G, elements)
                    if E == G:
                        probable.append((G, PossibleTypes.PROXIMITY_NO_OVERLAP))
                    else:
                        probable.append((G, PossibleTypes.PROXIMITY_GOOD_RATIO))
        tries += 1
        continue

    valid = []
    for C, t in probable:
        if C[0] < 0 or C[0] + C[2] > shape[1] or C[1] < 0 or C[1] + C[3] > shape[0]:
            continue
        else:
            valid.append((C, t))

    probable = valid
    return probable


def analyze_probable(probable, rank=0):
    if not probable:
        return None

    LOCAL_PERFECT = 1.0
    #MINSIZE = 2

    #perfect = 0
    #count = 0
    #for P, t in probable:
    #    if t not in [PossibleTypes.PROXIMITY_GOOD_RATIO, PossibleTypes.PROXIMITY_GOOD_RATIO]:
    #        perfect += P[3] / P[2]
    #        count += 1

    #if perfect > 0:
    #    LOCAL_PERFECT = perfect / count
    #print('Local perfect', LOCAL_PERFECT, perfect, count)

    #FIXME select the closest that fits criteria
    jumpship = False
    locked = False
    trigg = 0
    B = probable[0][0] # select first element
    last = probable[0][1]
    Y = B
    if last not in PossibleTypes.INITIAL_TYPES:
        jumpship = True
    for X, t in probable[1:]:
        if last == PossibleTypes.INITIAL_WITH_GOOD_RATIO and t == PossibleTypes.INITIAL_NO_OVERLAP:
            B = X
            Y = X
            last = PossibleTypes.INITIAL_NO_OVERLAP
            locked = True
        elif t in PossibleTypes.NO_OVERLAP_TYPES:
            h, Q1 = box_relation(X, (min(X[0], B[0]), B[1], max(X[0] + X[2], B[0] + B[2]), B[3]))
            v, Q2 = box_relation(X, (B[0], min(X[1], B[1]), B[2], max(X[1] + X[3], B[1] + B[3])))

            #if h == 0 or v == 0:
            if (X[2] >= B[2] and X[3] >= B[3]) and (h == 0 or v == 0) or (last in PossibleTypes.NO_OVERLAP_TYPES and (h == 0 or v == 0)):
                #if ((X[2] >= B[2] * 1.2 or X[3] >= B[3] * 1.2) and (h == 0 or v == 0)):# or (t == PossibleTypes.NO_OVERLAP and (h == 0 or v == 0)):
                #(last == PossibleTypes.PROXIMITY_GOOD_RATIO and (X[2] >= B[2] and X[3] >= B[3]) and (h == 0 or v == 0)) or
                #if ((X[2] >= B[2] * 1.2 or X[3] >= B[3] * 1.2) and (h == 0 or v == 0)) or (last == PossibleTypes.PROXIMITY_GOOD_RATIO and (X[2] >= B[2] and X[3] >= B[3]) and (h == 0 or v == 0)) or (t == PossibleTypes.NO_OVERLAP and (h == 0 or v == 0)):
                #if jumpship:
                #    break
                d, _ = box_relation(X, B)
                if d == 0:
                    B = X
                else:
                    jumpship = True
                Y = X
                last = t
                if t in PossibleTypes.SQUARED_TYPES:
                    if trigg >= rank:
                        break
                else:
                    trigg += 1
        elif t in PossibleTypes.ADEQUATE_TYPES:
            if not jumpship and not locked and X[2] * X[3] > B[2] * B[3] and abs(X[3] / X[2] - LOCAL_PERFECT) < abs(B[3] / B[2] - LOCAL_PERFECT):
                h, _ = box_relation(X, (min(X[0], B[0]), B[1], max(X[0] + X[2], B[0] + B[2]), B[3]))
                v, _ = box_relation(X, (B[0], min(X[1], B[1]), B[2], max(X[1] + X[3], B[1] + B[3])))
                if h == 0 or v == 0:
                    B = X
                    #if trigg >= rank:
                    #    locked = True
                    #else:
                    #    trigg += 1
                    last = t
                    trigg += 1
            elif last in PossibleTypes.NO_OVERLAP_TYPES:
                break
        # check that the selection has neighbours
        # select the best jump (1) that is not smaller that the previous selections
        # if no 1s, select the first selection

    if not jumpship:
        Y = B
    return Y


def detect_flow(X, elements, imageshape, contours, hierarchy, draw=False):
    #elements = sorted(elements,key=lambda x:box_relation(X,(x[0],x[1],x[2],x[3]))[0])
    elements = [x for x in elements if x[1] != 0 and x[1] + x[3] != imageshape[0] and x[0] != 0 and x[0] + x[2] != imageshape[1]]
    expected_collection_h, expected_collection_v = discovery([e[:6] for e in elements], X, imageshape)

    (x, y, w, h) = X

    all_y = [c for e in expected_collection_v for c in [e[1], e[1] + e[3]] if e[-1] in [State.B]]
    all_x = [c for e in expected_collection_h for c in [e[0], e[0] + e[2]] if e[-1] in [State.B]]

    if not all_y or not all_x:
        return Flow.INVALID, [], -1, (None, None, None, None)

    min_x = min(all_x)
    max_x = max(all_x)
    min_y = min(all_y)
    max_y = max(all_y)

    selected_elements = [x for x in elements if box_relation(x[:4], X)[0] == 0]
    selected_indexes = [x[4] for x in selected_elements]
    print('Selected', selected_indexes)

    ##########################################################################################################################################
    # CREATE HORIZONTAL TEMP
    ##########################################################################################################################################
    horizontal_draw = np.zeros(imageshape, dtype=np.uint8)
    miny = y
    maxy = y + h

    if expected_collection_h:
        for x9, y9, w9, h9, i9, e9 in expected_collection_h:
            miny = min(miny, y9)
            maxy = max(maxy, y9 + h9)
            if e9 == State.O and min_x <= x9 and max_x >= x9 + w9:
                cv2.drawContours(horizontal_draw, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
            #elif s9 == 1 and y9+h9 <= baseline:
            #    cv2.drawContours(horizontal_draw, contours, i9, Color.BLUE, -1,hierarchy=hierarchy,maxLevel=2)
            elif e9 == State.B:
                cv2.drawContours(horizontal_draw, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
            #    cv2.drawContours(horizontal_draw, contours, i9, Color.MAGENTA, -1,hierarchy=hierarchy,maxLevel=2)
            elif e9 == State.G:
                cv2.drawContours(horizontal_draw, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
            else:
                cv2.drawContours(horizontal_draw, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

        for i in selected_indexes:
            cv2.drawContours(horizontal_draw, contours, i, Color.RED, -1, hierarchy=hierarchy, maxLevel=2)

    ##########################################################################################################################################
    # CREATE VERTICAL TEMP
    ##########################################################################################################################################
    vertical_draw = np.zeros(imageshape, dtype=np.uint8)
    minx = x
    maxx = x + w
    if expected_collection_v:
        for x9, y9, w9, h9, i9, e9 in expected_collection_v:
            minx = min(minx, x9)
            maxx = max(maxx, x9 + w9)
            if e9 == State.O and min_y <= y9 and max_y >= y9 + h9:
                cv2.drawContours(vertical_draw, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
            elif e9 == State.B:
                cv2.drawContours(vertical_draw, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
            elif e9 == State.G:
                cv2.drawContours(vertical_draw, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
            else:
                cv2.drawContours(vertical_draw, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

        #for x9,y9,w9,h9,i9,a9 in arranged:
        #    if a9 <= 2:
        #        continue
        #    if x9 >= minx and x9+w9 <= maxx:
        #        cv2.drawContours(vertical_draw, contours, i9, Color.WHITE, -1,hierarchy=hierarchy,maxLevel=2)
        #cv2.drawContours(vertical_draw, contours, closest_index, Color.RED, -1,hierarchy=hierarchy,maxLevel=2)
        for i in selected_indexes:
            cv2.drawContours(vertical_draw, contours, i, Color.RED, -1, hierarchy=hierarchy, maxLevel=2)

    ##########################################################################################################################################
    # PROCEDURE
    ##########################################################################################################################################

    R = []
    L = []
    U = []
    D = []
    for E in expected_collection_h:
        if E[0] + E[2] < X[0] and E[-1] in [State.O, State.B]:
            L.append(E)
        elif E[0] > X[0] + X[2] and E[-1] in [State.O, State.B]:
            R.append(E)
    for E in expected_collection_v:
        if E[1] + E[3] < X[1] and E[-1] in [State.O, State.B]:
            U.append(E)
        elif E[1] > X[1] + X[3] and E[-1] in [State.O, State.B]:
            D.append(E)

    Ri = np.zeros(imageshape, dtype=np.uint8)
    Li = np.zeros(imageshape, dtype=np.uint8)
    Ui = np.zeros(imageshape, dtype=np.uint8)
    Di = np.zeros(imageshape, dtype=np.uint8)

    for x9, y9, w9, h9, i9, e9 in R:
        if e9 == State.O:
            cv2.drawContours(Ri, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.B:
            cv2.drawContours(Ri, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.G:
            cv2.drawContours(Ri, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
        else:
            cv2.drawContours(Ri, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

    for x9, y9, w9, h9, i9, e9 in L:
        if e9 == State.O:
            cv2.drawContours(Li, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.B:
            cv2.drawContours(Li, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.G:
            cv2.drawContours(Li, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
        else:
            cv2.drawContours(Li, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

    for x9, y9, w9, h9, i9, e9 in U:
        if e9 == State.O:
            cv2.drawContours(Ui, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.B:
            cv2.drawContours(Ui, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.G:
            cv2.drawContours(Ui, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
        else:
            cv2.drawContours(Ui, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

    for x9, y9, w9, h9, i9, e9 in D:
        if e9 == State.O:
            cv2.drawContours(Di, contours, i9, Color.ORANGE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.B:
            cv2.drawContours(Di, contours, i9, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
        elif e9 == State.G:
            cv2.drawContours(Di, contours, i9, Color.GREEN, -1, hierarchy=hierarchy, maxLevel=2)
        else:
            cv2.drawContours(Di, contours, i9, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

    #R = [x[:-1] for x in R]
    #L = [x[:-1] for x in L]
    #U = [x[:-1] for x in U]
    #D = [x[:-1] for x in D]

    #print('E',elements[0])
    #print('R',R[0])

    #L = sorted(L,key=lambda x:box_relation(X,(x[0],x[1],x[2],x[3]))[0])

    def check_validity(X, G):
        dist = max(X[2], X[3])
        d, Q = box_relation(X, G)
        #print('status', d, dist * 2)
        if d < dist * 2:
            if d == 0:
                if box_area(Q) // 3 > box_area(G):
                    return 2
            if X[2] * 0.8 < G[2] < X[2] * 1.2 and X[3] * 0.8 < G[3] < X[3] * 1.2:
                return 0
            else:
                return 1
        else:
            return 2

    def vicinity_checker(X, Q, Qi, collection):
        if Q:
            for E in Q:
                G = E[:4]
                probable = character_discovery(G, Q, imageshape)
                #print('PRO', probable)
                #for E in probable:
                #    G = E[0][:4]
                #    cv2.rectangle(Qi, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.YELLOW, 1)

                #G = analyze_probable(probable, rank=0)

                probable = sorted(probable, key=lambda x: box_relation(X, x[0])[0])
                for G in probable:
                    G = G[0]
                    state = check_validity(X, G)
                    if state == 0:
                        cv2.rectangle(Qi, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.RED, 1)
                        collection.append(G)
                        break
                    elif state == 2:
                        break

    vicinity_h = []
    vicinity_v = []
    vicinity_checker(X, R, Ri, vicinity_h)
    vicinity_checker(X, L, Li, vicinity_h)
    vicinity_checker(X, D, Di, vicinity_v)
    vicinity_checker(X, U, Ui, vicinity_v)

    collection = []
    spacing = -1
    h_k = h

    if vicinity_h and vicinity_v:
        h = min([box_relation(X, E)[0] for E in vicinity_h])
        v = min([box_relation(X, E)[0] for E in vicinity_v])
        print('h', h)
        print('v', v)
        if h < v:
            flow = Flow.HORIZONTAL
        else:
            flow = Flow.VERTICAL
    elif vicinity_h:
        h = min([box_relation(X, E)[0] for E in vicinity_h])
        flow = Flow.HORIZONTAL
    elif vicinity_v:
        v = min([box_relation(X, E)[0] for E in vicinity_v])
        flow = Flow.VERTICAL
    else:
        flow = Flow.INVALID
        print('INVALID')
        #return flow, collection, spacing, (min_x, max_x, min_y, max_y)

    if flow == Flow.VERTICAL:
        print('VERTICAL')
        collection.extend(U)
        collection.extend(D)
        collection = cleanup(collection, Flow.VERTICAL, min_y, max_y)
        collection.extend(selected_elements)
        spacing = v
    elif flow == Flow.HORIZONTAL:
        print('HORIZONTAL')
        collection.extend(L)
        collection.extend(R)
        collection = cleanup(collection, Flow.HORIZONTAL, min_x, max_x)
        collection.extend(selected_elements)
        spacing = h

    if draw:
        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.title(f"Horizontal")
        plt.imshow(horizontal_draw, cmap='gray')
        plt.axis('off')
        #cv2.imwrite('horizontal.png',cv2.cvtColor(horizontal_draw,cv2.COLOR_RGB2BGR))
        plt.axhline(y=y, xmin=0, color='r')
        plt.axhline(y=y + h_k, xmin=0, color='r')
        plt.axvline(x=min_x, ymin=0, color='b')
        plt.axvline(x=max_x, ymin=0, color='b')

        plt.subplot(142)
        plt.title(f"Vertical")
        plt.imshow(vertical_draw, cmap='gray')
        #cv2.imwrite('vertical.png',cv2.cvtColor(vertical_draw,cv2.COLOR_RGB2BGR))
        plt.axis('off')
        plt.axvline(x=x, ymin=0, color='r')
        plt.axvline(x=x + w, ymin=0, color='r')
        plt.axhline(y=min_y, xmin=0, color='b')
        plt.axhline(y=max_y, xmin=0, color='b')
        plt.show()

        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.title(f"L")
        #if L:
        #    plt.title(f"{abs((X[2]*X[3])-(Gl[2]*Gl[3]))/part}")
        plt.axis('off')
        plt.imshow(Li)

        plt.subplot(142)
        plt.title(f"R")
        #if R:
        #    plt.title(f"{abs((X[2]*X[3])-(Gr[2]*Gr[3]))/part}")
        plt.axis('off')
        plt.imshow(Ri)

        plt.subplot(143)
        plt.title(f"U")
        #if U:
        #    plt.title(f"{abs((X[2]*X[3])-(Gu[2]*Gu[3]))/part}")
        plt.axis('off')
        plt.imshow(Ui)

        plt.subplot(144)
        plt.title(f"D")
        #if D:
        #    plt.title(f"{abs((X[2]*X[3])-(Gd[2]*Gd[3]))/part}")
        plt.axis('off')
        plt.imshow(Di)
        plt.show()
    return flow, collection, spacing, (min_x, max_x, min_y, max_y)


def cleanup(collection, flow, min_pos, max_pos):
    new_collection = []
    for X in collection:
        if X[-1] == State.B:
            new_collection.append(X)
    return new_collection

#def parallel_search(X,direction,selected,elements,spacing,image,flow):


def parallel_search(X, elements, spacing, imageshape, flow, draw=False):

    #print('X',X)
    neighbor_line_h = np.zeros(imageshape, dtype=np.uint8)
    neighbor_line_v = np.zeros(imageshape, dtype=np.uint8)
    #complete = np.zeros_like(image)
#
    #for x2,y2,w2,h2 in OVP:
    #    cv2.rectangle(complete,(x2,y2),(x2+w2,y2+h2),Color.YELLOW,1)
#
    #cv2.rectangle(complete,(x,y),(x+w,y+h),Color.RED,1)

    up = []
    down = []
    left = []
    right = []

    max_h = 0
    max_w = 0

    (x, y, w, h) = X

    p_w = X[2]
    p_h = X[3]

    for x2, y2, w2, h2, ii, e2 in elements:#[closest_index:]:

        if y2 != 0 and y2 + h2 != imageshape[0] and x2 != 0 and x2 + w2 != imageshape[1]:

            if flow == Flow.VERTICAL and w2 > p_w * 1.2:
                continue
            if flow == Flow.HORIZONTAL and h2 > p_h * 1.2:
                continue

            #if LOW_RATIO < h2/w2 < HIGH_RATIO:
            if w * 0.9 < w2 < w * 1.2 or h * 0.9 < h2 < h * 1.2:
                color = Color.YELLOW
            else:
                color = Color.BLUE

            # UP
            if y2 + h2 < y - spacing and y2 > y - 2.5 * p_h:
                #cv2.rectangle(neighbor_line_h,(x2,y2),(x2+w2,y2+h2),Color.BLUE,1)
                cv2.rectangle(neighbor_line_h, (x2, y2), (x2 + w2, y2 + h2), color, 1)
                up.append((x2, y2, w2, h2, ii, e2))
                if w * 0.9 < w2 < w * 1.2 or h * 0.9 < h2 < h * 1.2:
                    max_h = max(max_h, h2)
            # DOWN
            if y2 > y + h + spacing and y2 + h2 < y + h + 2.5 * p_h:
                #cv2.rectangle(neighbor_line_h,(x2,y2),(x2+w2,y2+h2),Color.YELLOW,1)
                cv2.rectangle(neighbor_line_h, (x2, y2), (x2 + w2, y2 + h2), color, 1)
                down.append((x2, y2, w2, h2, ii, e2))
                if w * 0.9 < w2 < w * 1.2 or h * 0.9 < h2 < h * 1.2:
                    max_h = max(max_h, h2)
            # LEFT
            if x2 > x - 2.5 * p_w and x2 + w2 < x - spacing:
                #cv2.rectangle(neighbor_line_v,(x2,y2),(x2+w2,y2+h2),Color.ORANGE,1)
                cv2.rectangle(neighbor_line_v, (x2, y2), (x2 + w2, y2 + h2), color, 1)
                left.append((x2, y2, w2, h2, ii, e2))
                if w * 0.9 < w2 < w * 1.2 or h * 0.9 < h2 < h * 1.2:
                    max_w = max(max_w, w2)
                    #left.append(x2+w2//2)
                    #left.append()
            # RIGHT
            if x2 > x + w + spacing and x2 + w2 < x + w + 2.5 * p_w:
                #cv2.rectangle(neighbor_line_v,(x2,y2),(x2+w2,y2+h2),Color.CYAN,1)
                cv2.rectangle(neighbor_line_v, (x2, y2), (x2 + w2, y2 + h2), color, 1)
                right.append((x2, y2, w2, h2, ii, e2))
                if w * 0.9 < w2 < w * 1.2 or h * 0.9 < h2 < h * 1.2:
                    max_w = max(max_w, w2)

    #for x2,y2,w2,h2,ii in left:
    #    cv2.rectangle(complete,(x2,y2),(x2+w2,y2+h2),Color.BLUE,1)

    #adj = cv2.cvtColor(adj,cv2.COLOR_GRAY2RGB)
    #x2,y2,w2,h2 = WXYZ
    #max_w = max(max_w,w2)
    #cv2.rectangle(adj,(x2,y2),(x2+w2,y2+h2),Color.BLUE,1)
    #x2,y2,w2,h2 = HXYZ
    #max_h = max(max_h,h2)
    #cv2.rectangle(adj,(x2,y2),(x2+w2,y2+h2),Color.RED,1)

    if draw:
        plt.figure(figsize=(50, 20))
        #plt.subplot(141)
        #plt.title(f"ADJACANT")
        #plt.axis('off')
        ##plt.axhline(y=(mean_t-(max_h*1.1)//2),xmin=0,color='w')
        ##plt.axhline(y=(mean_b+(max_h*1.1)//2),xmin=0,color='w')
        ##plt.axvline(x=(mean_l-(max_w*1.1)//2),ymin=0,color='w')
        ##plt.axvline(x=(mean_r+(max_w*1.1)//2),ymin=0,color='w')
    #
        ##plt.axvline(x=x-2.5*p_w,ymin=0,color='g')
        ##plt.axvline(x=x-spacing,ymin=0,color='g')
    #
        #plt.axhline(y=y-2.5*p_h,xmin=0,color='y')
        #plt.axhline(y=y+h+2.5*p_h,xmin=0,color='y')
        #plt.axvline(x=x-2.5*p_w,ymin=0,color='y')
        #plt.axvline(x=x+w+2.5*p_w,ymin=0,color='y')
        #plt.imshow(adj,cmap='gray')

        plt.subplot(141)
        plt.title(f"HORIZONTAL")
        plt.axis('off')
        plt.axhline(y=y - 2.5 * p_h, xmin=0, color='y')
        plt.axhline(y=y + h + 2.5 * p_h, xmin=0, color='y')
        #plt.axhline(y=mean_t,xmin=0,color='r')
        #plt.axhline(y=mean_b,xmin=0,color='b')
        plt.imshow(neighbor_line_h)

        plt.subplot(142)
        plt.title(f"VERTICAL")
        plt.axis('off')
        plt.axvline(x=x - 2.5 * p_w, ymin=0, color='y')
        plt.axvline(x=x + w + 2.5 * p_w, ymin=0, color='y')
        #plt.axvline(x=mean_l,ymin=0,color='b')
        #plt.axvline(x=mean_r,ymin=0,color='r')
        plt.imshow(neighbor_line_v)

        #plt.subplot(144)
        #plt.title(f"COMPLETE")
        #plt.axis('off')
        #plt.imshow(complete)
        plt.show()

    return (max_w, max_h), left, right, up, down


def scan(direction, maxsize, spacing, X, aggregation, canvas, imageshape, contours, hierarchy, min_limit, max_limit):
    (x, y, w, h) = X

    #tmp = np.zeros_like(image)
    #for x2,y2,w2,h2,i2,e2 in aggregation:
    #    cv2.rectangle(tmp,(x2,y2),(x2+w2,y2+h2),Color.BLUE,1)
    #
    #plt.figure(figsize=(50,20))
    #plt.subplot(141)
    #plt.axis('off')
    #plt.imshow(tmp)
    #plt.show()

    pos = x if direction in [Direction.LEFT, Direction.RIGHT] else y
    size = w if direction in [Direction.LEFT, Direction.RIGHT] else h

    similar = 3 * maxsize
    if direction in [Direction.LEFT, Direction.UP]:
        second = pos - spacing
        first = second - maxsize
    elif direction in [Direction.RIGHT, Direction.DOWN]:
        first = pos + size + spacing
        second = first + maxsize
    f_c = 0
    s_c = 0
    A = []
    hits = 0
    for inc in range(0, int(2.5 * size - maxsize)):
        S = []
        if direction in [Direction.LEFT, Direction.UP]:
            first -= 1
            second -= 1
        elif direction in [Direction.RIGHT, Direction.DOWN]:
            first += 1
            second += 1
        f_c = second
        s_c = first
        c_hits = 0
        for P in aggregation:
            if direction in [Direction.LEFT, Direction.RIGHT]:
                if first * 0.9 <= P[0] and P[0] + P[2] <= second * 1.1:
                    S.append(P)
                    f_c = min(P[0], f_c)
                    s_c = max(P[0] + P[2], s_c)
                    if w * 0.9 < P[2] < w * 1.1 or h * 0.9 < P[3] < h * 1.1:
                        c_hits += 1
            elif direction in [Direction.UP, Direction.DOWN]:
                if first * 0.9 <= P[1] and P[1] + P[3] <= second * 1.1:
                    S.append(P)
                    f_c = min(P[1], f_c)
                    s_c = max(P[1] + P[3], s_c)
                    if w * 0.9 < P[2] < w * 1.1 or h * 0.9 < P[3] < h * 1.1:
                        c_hits += 1
        if c_hits > hits:
            A = S
            hits = c_hits
            similar = abs(maxsize - (s_c - f_c))
            #print('SAVED',similar,hits)
            #print(U)
            #print()
        elif c_hits == hits and abs(maxsize - (s_c - f_c)) < similar:
            A = S
            hits = c_hits
            similar = abs(maxsize - (s_c - f_c))
            #print('SAVED',similar,hits)
            #print(U)
            #print()

    if not A:
        A = aggregation
    A = sorted(A, key=lambda x: x[2] * x[3], reverse=True)

    #for X in A:
    #    cv2.rectangle(canvas,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.MAGENTA,1)

    try:
        if direction in [Direction.LEFT, Direction.RIGHT]:
            X = next((E for E in A if min_limit < E[1] < max_limit))
        elif direction in [Direction.UP, Direction.DOWN]:
            X = next((E for E in A if min_limit < E[0] < max_limit))
    except StopIteration:
        X = None

    if X is not None:
        Q = X

        probable = character_discovery(Q[:4], A, imageshape)
        G = analyze_probable(probable, rank=0)
        if G is None:
            return None, []

        A = sorted(aggregation, key=lambda x: box_relation(G[:4], x[:4])[0])
        hh, vv = discovery(A, G[:4], imageshape)

        if direction in [Direction.LEFT, Direction.RIGHT]:
            line = vv
        elif direction in [Direction.UP, Direction.DOWN]:
            line = hh
        for X in line:
            #if X[-1] == 0:
            #    cv2.rectangle(canvas,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.ORANGE,3)
            if X[-1] == State.B:
                #cv2.rectangle(canvas,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.BLUE,3)
                cv2.drawContours(canvas, contours, X[-2], Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)
            #if X[-1] == 2:
            #    cv2.rectangle(canvas,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.WHITE,3)
            #if X[-1] == 3:
            #    cv2.rectangle(canvas,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.MAGENTA,3)

        #cv2.rectangle(canvas,(Q[0],Q[1]),(Q[0]+Q[2],Q[1]+Q[3]),Color.RED,3)
        return G, line
    else:
        return None, []


def split(image, clusters):

    if np.isin(0, image[:, :, 3]):
        print("ALPHA")
        clusters += 1

    samples = image.reshape(-1, 4).astype(np.float32)

    retval, labels, centers = cv2.kmeans(samples, clusters, None, (cv2.TERM_CRITERIA_EPS, 30, 0.1), 3, cv2.KMEANS_PP_CENTERS)
    centers = np.uint8(centers)
    quant = centers[labels.flatten()]
    quant = quant.reshape((image.shape))

    colors = centers

    print('ALL COLORS', colors)

    index = np.where(np.all(colors == np.array([0, 0, 0, 0]), axis=1))
    colors = np.delete(colors, index, axis=0) # Remove excluded color

    print('REMAINING', colors)

    red = quant[:, :, 0]
    green = quant[:, :, 1]
    blue = quant[:, :, 2]
    alpha = quant[:, :, 3]

    splits = []
    if len(colors) < 2:
        return splits
    for c in colors:
        color1 = np.uint8((red == c[0]) & (green == c[1]) & (blue == c[2]) & (alpha == c[3])) * 255
        splits.append(color1)
    return splits


# this is actually line deduction (not character discovery)


def discovery(elements, X, imageshape):
    elements = sorted(elements, key=lambda x: box_relation(X, (x[0], x[1], x[2], x[3]))[0])
    H = X
    V = X
    #(x,y,w,h,a) = X
    (x, y, w, h) = X

    min_x = x
    max_x = x + w
    min_y = y
    max_y = y + h

    horizontal_spacing = []
    vertical_spacing = []
    expected_collection_h = []
    expected_collection_v = []

    p_w = w
    p_h = h

    #up_d = np.zeros_like(image)
    #cv2.rectangle(up_d,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.RED,3)
    for x2, y2, w2, h2, ii, e2 in elements:#[closest_index:]:
        B = (x2, y2, w2, h2)
        X = B
        if y2 <= y <= y2 + h2 or y2 <= y + h <= y2 + h2 or (y <= y2 and y2 + h2 <= y + h) or (y >= y2 and y2 + h2 >= y + h):
            if y2 != 0 and y2 + h2 != imageshape[0] and x2 != 0 and x2 + w2 != imageshape[1]:
                if y < return_center((B))[1] < y + h:
                    state = State.B
                    if h2 > h * 1.1 and not (y <= return_center(B)[1] <= y + h and h2 < h * 1.5):
                        continue

                    # LEFT
                    if x2 + w2 < min_x:
                        dist = abs(min_x - (x2 + w2))
                        if dist < 2.0 * w:
                            min_x = x2
                            if w2 > p_w:
                                p_w = w2
                            horizontal_spacing.append(dist)
                        else:
                            #print('LEFT')
                            state = State.G
                    # RIGHT
                    elif x2 > max_x:
                        dist = abs(max_x - x2)
                        if dist < 2.0 * w:
                            max_x = x2 + w2
                            if w2 > p_w:
                                p_w = w2
                            horizontal_spacing.append(dist)
                        else:
                            #print('RIGHT')
                            state = State.G
                    # OVERLAP TOWARDS RIGHT
                    elif x2 <= max_x and x2 + w2 >= max_x:
                        dist = abs(max_x - (x2 + w2))
                        if dist < w * 1.2:
                            max_x = max(x2 + w2, max_x)
                        else:
                            #print('OVER RIGHT')
                            state = State.G
                    # OVERLAP TOWARDS LEFT
                    elif x2 <= min_x and x2 + w2 >= min_x:
                        dist = abs(min_x - x2)
                        if dist < w * 1.2:
                            min_x = min(x2, min_x)
                        else:
                            #print('OVER LEFT')
                            state = State.G
                    # OVERLAP
                    elif x2 >= min_x and x2 + w2 <= max_x:
                        pass
                    else:
                        state = State.W
                else:
                    state = State.O
            else:
                state = State.W

            if state == State.B:
                if w2 > H[2] and h2 > H[3]:
                    H = B
            #if state == 0:
            #    cv2.rectangle(up_d,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.ORANGE,3)
            #if state == 1:
            #    cv2.rectangle(up_d,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.BLUE,3)
            #if state == 2:
            #    cv2.rectangle(up_d,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.WHITE,3)
            #if state == 3:
            #    cv2.rectangle(up_d,(X[0],X[1]),(X[0]+X[2],X[1]+X[3]),Color.GREEN,3)
            #plt.figure(figsize=(20,20))
            #plt.imshow(up_d)
            #plt.show()

            expected_collection_h.append((x2, y2, w2, h2, ii, state))
        if x2 <= x <= x2 + w2 or x2 <= x + w <= x2 + w2 or (x <= x2 and x + w >= x2 + w2) or (x >= x2 and x + w <= x2 + w2):
            if y2 != 0 and y2 + h2 != imageshape[0] and x2 != 0 and x2 + w2 != imageshape[1]:
                if x < return_center((B))[0] < x + w:
                    state = State.B
                    if w2 > w * 1.1 and not (x <= return_center(B)[0] <= x + w and w2 < w * 1.5):
                        continue

                    # TOP
                    if y2 + h2 < min_y:
                        dist = abs(min_y - (y2 + h2))
                        if dist < 2.0 * h:
                            min_y = y2
                            if h2 > p_h:
                                p_h = h2
                            vertical_spacing.append(dist)
                        else:
                            state = State.G
                    # BOTTOM
                    elif y2 > max_y:
                        dist = abs(max_y - y2)
                        if dist < 2.0 * h:
                            max_y = y2 + h2
                            if h2 > p_h:
                                p_h = h2
                            vertical_spacing.append(dist)
                        else:
                            state = State.G
                    # OVERLAP TOWARDS BOTTOM
                    elif y2 <= max_y and y2 + h2 >= max_y:
                        dist = abs(max_y - (y2 + h2))
                        if dist < h * 1.2:
                            max_y = max(y2 + h2, max_y)
                        else:
                            state = State.G
                    # OVERLAP TOWARDS TOP
                    elif y2 <= min_y and y2 + h2 >= min_y:
                        dist = abs(min_y - y2)
                        if dist < h * 1.2:
                            min_y = min(y2, min_y)
                        else:
                            state = State.G
                    # OVERLAP
                    elif y2 >= min_y and y2 + h2 <= max_y:
                        pass
                    else:
                        state = State.W
                else:
                    state = State.O
            else:
                state = State.W

            if state == State.B:
                if w2 > V[2] and h2 > V[3]:
                    V = B
            expected_collection_v.append((x2, y2, w2, h2, ii, state))
    return expected_collection_h, expected_collection_v


def generate_elements(full):
    print(full.dtype)
    contours, hierarchy = cv2.findContours(full, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    if not contours:
        print('No contours')
        return [], [], []

    elements = []
    factor = 5
    for i in range(len(contours)):
        if hierarchy[0][i][3] == -1:
            #if select is not None:
            #    dist = cv2.pointPolygonTest(contours[i],select,True)
            #    if dist > 0.0:
            #        dist = 0.0
            #else:
            #    dist = 0
            x, y, w, h = cv2.boundingRect(contours[i])
            a = cv2.contourArea(contours[i])

            # FIXME quick fix when it comes to large thin elements
            count = 0
            if w > h * factor or h > w * factor:
                empty = np.zeros((h, w), np.uint8)
                cv2.drawContours(empty, contours, i, 255, offset=(-x, -y))
                #plt.figure(figsize=(50, 20))
                #plt.subplot(141)
                #plt.title(f'{w},{h}')
                #plt.axis('off')
                #plt.imshow(empty, cmap='gray')
                #plt.show()
                empty = empty / 255
                for line in empty:
                    count += np.sum(line)
                if count < (w * h) / 10:
                    continue
            elements.append((x, y, w, h, i, a))
    return contours, hierarchy, elements


def find_closest(elements, contours, select, imageshape):
    distances = []

    empty = np.zeros(imageshape[:2])

    for Q in elements:
        dist = cv2.pointPolygonTest(contours[Q[4]], select, True)
        cv2.rectangle(empty, (Q[0], Q[1]), (Q[0] + Q[2], Q[1] + Q[3]), 255)
        if dist > 0.0:
            dist = 0.0
        distances.append((*Q, abs(dist)))

    #plt.figure(figsize=(50, 20))
    #plt.subplot(141)
    #plt.axis('off')
    #circle = plt.Circle(select, 4, color='r')
    #plt.gcf().gca().add_artist(circle)
    #plt.imshow(empty, cmap='gray')
    #plt.show()

    distances = sorted(distances, key=lambda x: x[-1])
    closest_index = 0
    composite = False
    if len(distances) < 1:
        print('NOT ENOUGH CHOICES')
        return None, None, None, None, None

    factor = 10
    Q = None

    while True:
        x, y, w, h, i, a, d = distances[closest_index]
        D = (x, y, w, h)
        if Q is None:
            Q = distances[closest_index]
        elif box_point_test(D, select):
            if a < Q[-2] and a > MIN_SIZE:
                Q = distances[closest_index]
        else:
            if Q:
                break
        if (x == 0 or x + w == imageshape[1] or y == 0 or y + h == imageshape[0] or np.isclose(a, 0.0)):
            if distances[closest_index][-1] < 5:
                composite = True
        elif imageshape[0] // factor < h or imageshape[1] // factor < w:
            if distances[closest_index][-1] < 5:
                composite = True
        closest_index += 1
        if closest_index > len(distances) - 1:
            if not composite:
                print(f"No suitable contour found")
                return None, None, None, None, None
            else:
                break

    x, y, w, h, i, a, d = Q
    Q = (x, y, w, h) # SAVE INITIAL BOX
    #print('DISTANCE', distances[closest_index][-1])
    print('DISTANCE', d, 'COMPOSITE', composite)
    #return Q, distances[closest_index][4], distances[closest_index][-1], [e[:-1] for e in distances], composite
    return Q, i, d, [e[:-1] for e in distances], composite


def reject_outliers(data, m=1):
    return data[abs(data - np.mean(data)) < m * np.std(data)]


def analyze_split(title, image, full, select, sharp=False, erode=0, forced_dilation=0, queue=None, draw=False, prev_composite=0):
    #global datapoints
    #global save_for_later
    #global save_full
    #global forced_dilation
    #global imgselect
    #global grayscale

    original_draw = draw

    ############################
    #draw = False
    ############################

    main_title = f'{title}'

    if len(main_title) > 2 * maxdepth:
        print(f'\x1b[1;31mEXCESSIVE SEARCH\x1b[0m')
        return None

    print()
    print('#' * 20)
    print(main_title)
    #print('VALID',valid)

    #############################################################################################################################################################
    # PERFORM PREPROCESSING
    #############################################################################################################################################################

    if erode > 0 and forced_dilation == 0:
        element = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        full = cv2.morphologyEx(full, cv2.MORPH_ERODE, element, iterations=erode)
        print('ERODE')
    elif forced_dilation > 0:
        element = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        full = cv2.morphologyEx(full, cv2.MORPH_DILATE, element, iterations=forced_dilation)
        forced_dilation = 0
        print('FORCED DILATION')

    #############################################################################################################################################################
    # GENERATE ELEMENTS AND CALCULATE DISTANCES FROM SEED POINT
    #############################################################################################################################################################

    if draw:
        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.axis('off')
        plt.title('FULL ' + main_title)
        plt.imshow(full, cmap='gray')
        plt.show()

    contours, hierarchy, elements = generate_elements(full)

    #############################################################################################################################################################
    # FIND CLOSEST
    #############################################################################################################################################################

    Q, closest_index, distance_from, elements, composite = find_closest(elements, contours, select, image.shape)

    if Q is None:
        print('NO ELEMENT IN VICINITY')
        return None

    (x, y, w, h) = Q

    A = Q

    #save_for_later = (contours,hierarchy,elements,image,Q)

    #######################################################################################
    # DRAWING
    #######################################################################################

    rects_h = np.zeros_like(image)
    rects_v = np.zeros_like(image)

    for xA, yA, wA, hA, iA, aA in elements:
        if yA == 0 or yA + hA == image.shape[0] or xA == 0 or xA + wA == image.shape[1]:
            cv2.rectangle(rects_h, (xA, yA), (xA + wA, yA + hA), Color.WHITE, 1)
            cv2.rectangle(rects_v, (xA, yA), (xA + wA, yA + hA), Color.WHITE, 1)
        else:
            cv2.rectangle(rects_h, (xA, yA), (xA + wA, yA + hA), Color.BLUE, 1)
            cv2.rectangle(rects_v, (xA, yA), (xA + wA, yA + hA), Color.BLUE, 1)

    #######################################################################################
    # SELECTION
    #######################################################################################

    #select = imagepoint(rects_h,imgselect)[0]
    #select = (389,224)
    #select = (107,208)
    #select = (196,110)
    #select = (276,111)

    #xs,ys = select
#
    #for xA,yA,wA,hA,iA,aA in elements:
    #    if yA != 0 and yA+hA != image.shape[0] and xA != 0 and xA+wA != image.shape[1]:
    #        if xA < xs < xA+wA and yA < ys < yA+hA:
    #            X = (xA,yA,wA,hA,iA,aA)
    #            break
#
    #A = X[:4]

    #######################################################################################
    # CHARACTER DISCOVERY
    #######################################################################################

    print('A', A)

    probable = character_discovery(A, elements, image.shape)

    print('PROBABLE', probable)

    X = analyze_probable(probable, rank=0)
    if X is None and not composite:
        print(f'\x1b[1;31mNO PROBABLE X FOUND\x1b[0m')
        return None
    elif X:
        print('X is ', X)
    else:
        X = None
        flow = Flow.INVALID

    if X:
        #######################################################################################
        # DRAWING
        #######################################################################################

        SCALE = 3
        (x, y, w, h) = A
        #ratio = h / w
        radius_of_observation = (max(w, h) // 2) * SCALE
        #elements = sorted(elements,key=lambda x:box_relation(A,(x[0],x[1],x[2],x[3]))[0])
        #print('ELEMENT RATIO',ratio)

        center = return_center((x, y, w, h))
        cv2.rectangle(rects_h, (x, y), (x + w, y + h), Color.RED, 1)
        cv2.circle(rects_h, center, radius_of_observation // SCALE, Color.CYAN, 1)
        cv2.circle(rects_h, center, radius_of_observation, Color.RED, 1)
        #print('PROBABLE',probable)

        best = rects_v.copy()

        for M, _ in probable:
            cv2.rectangle(rects_v, (M[0], M[1]), (M[0] + M[2], M[1] + M[3]), Color.YELLOW, 1)

        cv2.rectangle(best, (X[0], X[1]), (X[0] + X[2], X[1] + X[3]), Color.RED, 1)
        #######################################################################################
        # OTHER
        #######################################################################################

        empty = np.zeros_like(image)
        step = 255 // len(elements)
        step = max(1, step)
        current = 0
        for _, _, _, _, ix, _ in elements:
            cv2.drawContours(empty, contours, ix, (255 - current, 0, current), -1, hierarchy=hierarchy, maxLevel=2)
            current += step
            current = min(255, current)

        ##########################
        draw = False
        ##########################

        #print('PLOT ERROR')

        if draw:
            plt.figure(figsize=(50, 20))
            plt.subplot(141)
            plt.axis('off')
            plt.title('DISTANCES')
            plt.imshow(empty)
            ##cv2.imwrite('distances.png',cv2.cvtColor(empty,cv2.COLOR_RGB2BGR))
            print(empty.shape,empty.ndim)
            plt.subplot(142)
            plt.axis('off')
            plt.title('BEST')
            plt.imshow(best)
            plt.show()

            plt.figure(figsize=(50, 20))
            plt.subplot(141)
            plt.axis('off')
            plt.title('BOXES')
            plt.imshow(rects_h)
            ###cv2.imwrite('first.png',cv2.cvtColor(rects_h,cv2.COLOR_RGB2BGR))
            print(rects_h.shape,rects_h.ndim)
            #plt.show()

            #plt.figure(figsize=(50, 20))
            plt.subplot(142)
            plt.axis('off')
            plt.title('SELECTIONS')
            plt.imshow(rects_v)
            ###cv2.imwrite('second.png',cv2.cvtColor(rects_v,cv2.COLOR_RGB2BGR))
            print(rects_v.shape,rects_v.ndim)
            plt.show()

            ###plt.subplot(144)
            ###plt.axis('off')
            ###plt.title('EXTRACT')
            ###plt.imshow(extract)

            #plt.figure(figsize=(50, 20))
            #plt.subplot(141)
            #plt.axis('off')
            #plt.title('BEST')
            #plt.imshow(best)
            #print(best.shape,best.ndim)
            #plt.show()

        ###########################
        draw = original_draw
        ###########################

        flow, collection, spacing, (min_x, max_x, min_y, max_y) = detect_flow(X, elements, image.shape, contours, hierarchy, draw=draw)

    #from IPython.core.debugger import Pdb; Pdb().set_trace()

    if flow == Flow.INVALID:
        print(f'\x1b[1;31mINVALID FLOW {main_title}\x1b[0m')
        if composite and prev_composite < 2:

            ###########################
            draw = False
            ###########################

            print(f'\x1b[1;33mCOMPOSITE\x1b[0m')
            image_alpha = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
            selected_alpha = cv2.bitwise_and(image_alpha, image_alpha, mask=full)
            if draw:
                plt.figure(figsize=(50, 20))
                plt.subplot(141)
                plt.axis('off')
                plt.title('ALPHA')
                plt.imshow(selected_alpha)
                plt.subplot(142)
                plt.axis('off')
                plt.title('FULL')
                plt.imshow(full, cmap='gray')
                plt.show()
            threads = []
            print(f'\x1b[1;33mPREVCOMPOSITE #{prev_composite}\x1b[0m')

            #################################
            draw = original_draw
            #################################


            for i, c in enumerate(split(selected_alpha, 2)):
                t = multiprocessing.Process(target=analyze_split, args=(main_title + '-' + str(i + 1), image, c, select), kwargs={'sharp': sharp, 'erode': erode, 'queue': queue, 'draw': draw, 'prev_composite': prev_composite + 1})
                t.start()
                if sequential:
                    t.join()
                threads.append(t)
            for t in threads:
                t.join()
        return None

    main = np.zeros_like(image)
    for _, _, _, _, ix, _ in collection:
        cv2.drawContours(main, contours, ix, Color.WHITE, -1, hierarchy=hierarchy, maxLevel=2)

    if draw:
        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.title(f"MAIN")
        plt.axis('off')
        plt.imshow(main)
        plt.show()

    (max_w, max_h), left, right, up, down = parallel_search(X, elements, spacing, image.shape, flow, draw=draw)

    up_d = np.zeros_like(image)
    down_d = np.zeros_like(image)
    left_d = np.zeros_like(image)
    right_d = np.zeros_like(image)

    #######################################
    draw = original_draw
    #######################################

    if draw:
        plt.figure(figsize=(50, 20))

    #print('LEFT')
    G, line = scan(Direction.LEFT, max_w, spacing, X, left, left_d, image.shape, contours, hierarchy, min_y, max_y)
    print('G L', G)
    if draw:
        plt.subplot(141)
        plt.title(f"LEFT")
        plt.axis('off')
        plt.imshow(left_d)

    #print('RIGHT')
    G, line = scan(Direction.RIGHT, max_w, spacing, X, right, right_d, image.shape, contours, hierarchy, min_y, max_y)
    print('G R', G)
    if draw:
        plt.subplot(142)
        plt.title(f"RIGHT")
        plt.axis('off')
        plt.imshow(right_d)

    #print('UP')
    G, line = scan(Direction.UP, max_h, spacing, X, up, up_d, image.shape, contours, hierarchy, min_x, max_x)
    print('G U', G)
    if draw:
        plt.subplot(143)
        plt.title(f"UP")
        plt.axis('off')
        plt.imshow(up_d)

    #print('DOWN')
    G, line = scan(Direction.DOWN, max_h, spacing, X, down, down_d, image.shape, contours, hierarchy, min_x, max_x)
    print('G D', G)
    if draw:
        plt.subplot(144)
        plt.title(f"DOWN")
        plt.axis('off')
        plt.imshow(down_d)

    if draw:
        plt.show()

    #(max_w,max_h),left,right,up,down = parallel_search(G,elements,spacing,image,flow)
    #extra = np.zeros_like(image)
    ##cv2.rectangle(extra,(G[0],G[1]),(G[0]+G[2],G[1]+G[3]),Color.RED,1)
    #scan(Direction.DOWN,max_h,spacing,G,down,extra,image,contours,hierarchy,min_x,max_x)
    #plt.figure(figsize=(50,20))
    #plt.subplot(141)
    #plt.title(f"EXTRA")
    #plt.axis('off')
    #plt.imshow(extra)
    #plt.show()

    if flow == Flow.HORIZONTAL:
        new = cv2.bitwise_or(main, up_d)
        new = cv2.bitwise_or(new, down_d)
    elif flow == Flow.VERTICAL:
        new = cv2.bitwise_or(main, left_d)
        new = cv2.bitwise_or(new, right_d)
    else:
        raise

    new = cv2.cvtColor(new, cv2.COLOR_RGB2GRAY)

    if draw:
        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.title(f"FULL")
        plt.axis('off')
        plt.imshow(new, cmap='gray')
        plt.show()

    if erode > 0 or forced_dilation > 0:
        if forced_dilation > 0:
            erode = forced_dilation
        element = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        new = cv2.morphologyEx(new, cv2.MORPH_DILATE, element, iterations=erode)

    if flow == Flow.VERTICAL:
        new = cv2.rotate(new, cv2.ROTATE_90_COUNTERCLOCKWISE)
        old_select = select
        select = (select[1], new.shape[0] - select[0])

    print('ENTERING')

    ####################################################
    draw = original_draw
    ####################################################


    imagesize, elm, constraints, cont, hier = segment(new, select, 5, flow=flow, single=False, draw=draw, expected=X)

    ####################################################
    draw = original_draw
    ####################################################


    if elm:
        aggr = transcribe(imagesize, elm, cont, hier, draw=draw)
        if aggr is None:
            aggr = Aggregator(0)
    else:
        aggr = Aggregator(0)

    save_new = (new, select, flow)
    aggr.constraints = constraints
    aggr.extra = save_new
    aggr.name(main_title)


    #def quality(datapoints, forced_dilation):
    #    if len(datapoints) < 2:
    #        return None
    #    conf1 = np.mean(np.array([x['conf'] for x in datapoints[0]]))
    #    conf2 = np.mean(np.array([x['conf'] for x in datapoints[1]]))
    #    if max(conf1, conf2) > 80:
    #        #forced_dilation = 1
    #        pass

    #    print('CONF', conf1, conf2)
    #    print('MAIN', (conf1 + conf2) / 2)
    #    text1 = "".join([x['literal'] if x['conf'] >= 90 else '〇' for x in datapoints[0]])
    #    text2 = "".join([x['literal'] if x['conf'] >= 90 else '〇' for x in datapoints[1]])
    #    c1 = len(text1) - text1.count('〇')
    #    c2 = len(text2) - text2.count('〇')
    #    p1 = c1 / len(text1)
    #    p2 = c2 / len(text2)
    #    f = (p1 + p2) / 2
    #    return f

    #if aggr is not None:
    #    call = input('Continue...\n')
    #    if call == 'n':
    #        #raise
    #        main_title = "#"*50
    #    if call == 's':
    #        return None
    #        #raise

    id = uuid.uuid4().hex
    print(f'\x1b[1;32mTOOK {id}\x1b[0m')
    #mainaggr = queue.get()
    items, lock = queue
    lock.acquire()
    mainaggr = items[0]
    mainaggr = mainaggr | aggr
    items[0] = mainaggr
    #queue.put(mainaggr)
    lock.release()
    queue = (items, lock)
    print(f'\x1b[1;32mRETURNED {id}\x1b[0m')

    if mainaggr.parent(main_title):
        print(f'\x1b[1;31mTERMINATE BASED ON PARENT {mainaggr.title} {main_title}\x1b[0m', aggr.collapse())
        return None
    elif not mainaggr.deeper(main_title):
        print('\x1b[1;31mEXCEEDED DEPTH BASED ON PREVIOUS BEST\x1b[0m')
        return None
    else:
        print(f'\x1b[1;33mRELATION {mainaggr.title} {main_title}\x1b[0m')
        size_a, rating_a = mainaggr.value
        if size_a == len(mainaggr) and rating_a > 85:
            print(f'\x1b[1;31mQUICK TERMINATION\x1b[0m')
            return None
        #save_full = (full,select,flow)
        #mainaggr.append(save_full)
        #queue.put(mainaggr)
        #print('SAVED')

    if flow == Flow.VERTICAL:
        select = old_select
    image_alpha = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
    selected_alpha = cv2.bitwise_and(image_alpha, image_alpha, mask=full)

    #for i,c in enumerate(split(selected_alpha,2)):
    #    analyze_split(main_title+'-'+str(i+1),image,c,select)

    threads = []
    for i, c in enumerate(split(selected_alpha, 2)):
        t = multiprocessing.Process(target=analyze_split, args=(main_title + '-' + str(i + 1), image, c, select), kwargs={'sharp': sharp, 'erode': erode, 'queue': queue, 'draw': draw})
        t.start()
        if sequential:
            t.join()
        threads.append(t)
    for t in threads:
        t.join()
    return None


def single_shot(image, full, select, sharp=False, erode=False, draw=False):
    aggr = Aggregator(0)
    lock = multiprocessing.Lock()
    manager = multiprocessing.Manager()
    items = manager.list()
    items.append(aggr)
    queue = (items, lock)

    t = multiprocessing.Process(target=analyze_split, args=('X', image, full, select), kwargs={'sharp': sharp, 'erode': erode, 'queue': queue, 'draw': draw})
    t.start()
    t.join()

    aggr = queue[0][0]
    return aggr


def continue_extraction(aggr, image, sharp=False, erode=False, draw=False):
    main_title = aggr.title
    full, select, _ = aggr.extra
    lock = multiprocessing.Lock()
    manager = multiprocessing.Manager()
    items = manager.list()
    items.append(aggr)
    queue = (items, lock)

    image_alpha = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
    selected_alpha = cv2.bitwise_and(image_alpha, image_alpha, mask=full)

    threads = []
    for i, c in enumerate(split(selected_alpha, 2)):
        t = multiprocessing.Process(target=analyze_split, args=(main_title + '-' + str(i + 1), image, c, select), kwargs={'sharp': sharp, 'erode': erode, 'queue': queue, 'draw': draw})
        t.start()
        if sequential:
            t.join()
        threads.append(t)
    for t in threads:
        t.join()

    aggr = queue[0][0]
    return aggr


def extraction(image, select, sharp=False, erode=False, draw=False):

    if sharp:
        kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        image = cv2.filter2D(image, -1, kernel)

    kernel = np.ones((3, 3), np.uint8)
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    gray = cv2.threshold(gray, -1, 256, cv2.THRESH_OTSU)[1]
    edges = cv2.morphologyEx(gray, cv2.MORPH_GRADIENT, kernel)

    if draw:
        plt.figure(figsize=(50, 20))
        plt.subplot(141)
        plt.title("Show " + str(image.shape[:2]))
        plt.axis('off')
        plt.imshow(image)

        plt.subplot(142)
        plt.title("Gradient")
        circle = plt.Circle(select, 4, color='r')
        plt.gcf().gca().add_artist(circle)
        plt.axis('off')
        plt.imshow(edges, cmap="gray")

    if preliminary:
        contours, hierarchy = cv2.findContours(gray, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        empty = np.zeros_like(gray, np.uint8)
        for i in range(len(contours)):
            #M = cv2.moments(c)
            #cX = int(M["m10"] / M["m00"])
            #cY = int(M["m01"] / M["m00"])
            #if hierarchy[0][i][3] == -1:
            #hull = cv2.convexHull(contours[i])
            #dist = cv2.pointPolygonTest(hull, select, True)
            dist = cv2.pointPolygonTest(contours[i], select, True)
            #a = cv2.contourArea(contours[i])
            if dist >= 0:
                #cv2.drawContours(empty, contours, i, Color.BLUE, -1, hierarchy=hierarchy, maxLevel=2)
                cv2.fillPoly(empty, [contours[i]], 255)

        if draw:
            plt.subplot(143)
            plt.title("Preliminary Locations")
            circle = plt.Circle(select, 4, color='r')
            plt.gcf().gca().add_artist(circle)
            plt.axis('off')
            plt.imshow(empty, cmap="gray")
            plt.show()

            #plt.subplot(143)
            #plt.title("Canny")
            #c = auto_canny(i)
            #plt.axis('off')
            ##grayscale = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
            ##plt.imshow(grayscale,cmap="gray")
            #plt.imshow(c,cmap="gray")

            #plt.title("Sharpened")
            #plt.axis('off')
            #kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
            #sharp = cv2.filter2D(image, -1, kernel)
            #plt.imshow(sharp,cmap="gray")

        aggr = Aggregator(0)
        aggr.name('Y')
        aggr.extra = empty, select, None
        aggr = continue_extraction(aggr, image, sharp=sharp, erode=erode, draw=draw)
        size_a, rating_a = aggr.value
        if aggr.collapse():
            if rating_a > 70:
                return aggr

    else:
        plt.show()


    # ratio, area , distance, coverage # this is also stated at the bottom
    #valid = [np.inf,np.inf,np.inf,0]
    #best_image = None

    #lab = cv2.cvtColor(image, cv2.COLOR_RGB2LAB)

    #for i,c in enumerate(split(img,2)):
    #    analyze_split(str(i+1),image,c,select,sharp=sharp,erode=erode)

    #import multiprocessing.managers
    lock = multiprocessing.Lock()
    #manager = multiprocessing.managers.BaseManager()
    #manager.register('Aggregator', Aggregator)
    #manager.start()
    manager = multiprocessing.Manager()
    #aggr = manager.Aggregator(0)
    items = manager.list()
    aggr = Aggregator(0)
    items.append(aggr)
    #queue.put(aggr)
    queue = (items, lock)

    img = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
    threads = []
    for i, c in enumerate(split(img, 2)):
        t = multiprocessing.Process(target=analyze_split, args=(str(i + 1), image, c, select), kwargs={'sharp': sharp, 'erode': erode, 'queue': queue, 'draw': draw})
        t.start()
        if sequential:
            t.join()
        threads.append(t)

    for t in threads:
        t.join()

    #if draw:
        #plt.figure(figsize=(50,20))
        #plt.subplot(141)
        #plt.title('Selected')
        #plt.imshow(best_image)
        #plt.axis('off')
        #plt.show()

    #return best_image
    aggr = queue[0][0]
    return aggr
