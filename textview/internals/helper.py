import cv2
import numpy as np
from .relations import box_relation, return_center
from .constants import Color


def remove_border_from_img(img, select=None):
    def limits(image):
        small = 0
        large = image.shape[1]
        for x in range(image.shape[1] // 2):
            if np.all(image[:, x, :] == Color.BLACK) or np.all(image[:, x, :] == Color.WHITE):
                small = x if abs(small - x) == 1 else small
            if np.all(image[:, image.shape[1] - 1 - x, :] == Color.BLACK) or np.all(image[:, image.shape[1] - 1 - x, :] == Color.WHITE):
                large = image.shape[1] - 1 - x if abs(large - (image.shape[1] - 1 - x)) == 1 else large
            if small < x and large > (image.shape[1] - 1 - x):
                break

        if small != 0:
            return small + 1, large
        else:
            return 0, large

    left, right = limits(img)
    second = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    up, down = limits(second)

    if select is None:
        return img[up:down, left:right, :]
    else:
        select = (select[0] - left, select[1] - up)
        return img[up:down, left:right, :], select


def is_grayscale(image):
    b, g, r = image[:, :, 0], image[:, :, 1], image[:, :, 2]
    grayscale = True if (b == g).all() and (b == r).all() else False
    return grayscale


def interpolate(image):
    import cv2
    print('SIZE', image.shape)
    size = list(np.array(image.shape[:2][::-1]) * 2)
    size = tuple(size)
    print('SIZE', size)
    image_lanc = cv2.resize(image, dsize=size, interpolation=cv2.INTER_LANCZOS4)
    image = image_lanc
    return image


def rescale_select(select):
    print('SELECT', select)
    select = tuple(np.array(select) * 2)
    print('SELECT', select)
    return select


def imagepoint(img):

    class CoordinateStore:
        def __init__(self):
            self.points = ()
            #self.imgselect = imgselect
            self.run = True

        def select_point(self, event, x, y, flags, param):
            if event == cv2.EVENT_LBUTTONDOWN:
                #print(f"'{imgselect}':\n[\n({x},{y}), #{self.imgselect} \n],")
                print(f"[\n({x},{y})\n],")
                self.points = (x, y)
                self.run = False

    store = CoordinateStore()

    cv2.namedWindow('Image')
    cv2.setMouseCallback('Image', store.select_point)

    imgo = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    while store.run:
        cv2.imshow('Image', imgo)

        k = cv2.waitKey(10)

        if k & 0xFF == 27:
            break

    cv2.destroyAllWindows()
    return store.points


def contour_removal(full, namedwindow='Image'):

    img = np.zeros_like(full)

    class Selector(object):

        def __init__(self, full, namedwindow):
            self.drawing = False
            self.namedwindow = namedwindow
            self.sx, self.sy = -1, -1
            self.ex, self.ey = -1, -1
            self.x, self.y = -1, -1
            self.contours, self.hierarchy = cv2.findContours(full, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
            self.boxes = []
            for i in range(len(self.contours)):
                x, y, w, h = cv2.boundingRect(self.contours[i])
                a = cv2.contourArea(self.contours[i])
                self.boxes.append((x, y, w, h, a, i))
            self.valid = [i for i in range(0, len(self.contours))]
            self.removed = []
            self.full = full.copy()
            self.img = np.zeros_like(self.full)
            for i in self.valid:
                if self.hierarchy[0][i][3] == -1:
                    cv2.drawContours(self.img, self.contours, i, 255, -1, hierarchy=self.hierarchy, maxLevel=2)

        def return_img(self):
            self.img = np.zeros_like(self.full)
            for i in self.valid:
                if self.hierarchy[0][i][3] == -1:
                    cv2.drawContours(self.img, self.contours, i, 255, -1, hierarchy=self.hierarchy, maxLevel=2)
            if self.drawing:
                cv2.rectangle(self.img, (self.sx, self.sy), (self.x, self.y), 255, 1)
            return self.img

        def create_selection(self, event, x, y, flags, param):
            alternative = False
            if flags == cv2.EVENT_FLAG_ALTKEY + cv2.EVENT_FLAG_LBUTTON:
                alternative = True

            if event == cv2.EVENT_LBUTTONDOWN:
                self.drawing = True
                self.sx = x
                self.sy = y

            elif event == cv2.EVENT_MOUSEMOVE:
                self.x = x
                self.y = y
                if self.drawing:
                    self.tmp = self.img.copy()
                    cv2.rectangle(self.tmp, (self.sx, self.sy), (self.x, self.y), 255, 1)
                    cv2.imshow(namedwindow, self.tmp)

            elif event == cv2.EVENT_LBUTTONUP:
                self.drawing = False
                self.ex = x
                self.ey = y

                minx, maxx = sorted([self.sx, self.ex])
                miny, maxy = sorted([self.sy, self.ey])
                selected = []
                if max(maxx - minx, maxy - miny) < 5:
                    for X in self.boxes:
                        d = cv2.pointPolygonTest(self.contours[X[-1]], (self.ex, self.ey), True)
                        if d >= 0.0:
                            selected.append((d, X[-1]))

                    # selected element = (d, i)
                    selected = sorted(selected, key=lambda x: x[0])
                    for m in selected:
                        if m[-1] in self.valid:
                            if alternative:
                                fullset = set(self.valid)
                                removal = set([m[-1]])
                                removal = list(fullset.difference(removal))
                                for e in removal:
                                    self.valid.remove(e)
                                self.removed.append(removal)
                            else:
                                self.valid.remove(m[-1])
                                self.removed.append([m[-1]]) # put into list prior to appending
                        break
                else:
                    box = (minx, miny, maxx - minx, maxy - miny)
                    for X in self.boxes:
                        d, Q = box_relation(box, X[:4])
                        if d == 0:
                            selected.append(X)

                    # selected element = (x, y, w, h, a, i)
                    selected = sorted(selected, key=lambda x: X[2] * X[3])
                    collect = []
                    if alternative:
                        fullset = set(self.valid)
                        removal = set([x[-1] for x in selected])
                        removal = list(fullset.difference(removal))
                        for e in removal:
                            self.valid.remove(e)
                        collect = removal
                    else:
                        for m in selected:
                            if m[-1] in self.valid:
                                self.valid.remove(m[-1])
                                collect.append(m[-1])
                    self.removed.append(collect)

    selector = Selector(full, namedwindow)
    cv2.setMouseCallback(namedwindow, selector.create_selection)

    while True:
        img = selector.return_img()
        cv2.imshow(namedwindow, img)

        key = cv2.waitKey(1)
        if key & 0xFF == 27:
            break
        elif key & 0xFF == ord('u'):
            if selector.removed:
                selector.valid.extend(selector.removed.pop())

    size = 0
    select = None
    for i in selector.valid:
        box = next(b for b in selector.boxes if b[-1] == i)
        if box[3] * box[2] > size:
            select = return_center(box[:4])
            size = box[3] * box[2]
    return img, select
