import os
import ast


class SelectTranscript(object):
    def __init__(self):
        self.select = None
        self.transcript = None
        self.full_location = None


class TextViewFile(object):
    def __init__(self, f, root='/tmp/textview'):
        f = os.path.join(root, f)
        self.location = f
        self.data = []
        text = open(f).readlines()
        st = SelectTranscript()
        for i, line in enumerate(text):
            split = line.split(' -:- ')
            if split[0] != "#" * 32:
                if i == 0:
                    loc = os.path.join(root, split[0] + '.bmp')
                    self.main = loc
                    select = ast.literal_eval(split[1].split(': ')[1][:-1])
                    st.select = select
                else:
                    loc = os.path.join(root, split[0] + '.jpg')
                    st.full_location = loc
                    transcript = ast.literal_eval(split[1][:-1])
                    st.transcript = transcript
                    self.data.append(st)
                    st = SelectTranscript()
            else:
                select = ast.literal_eval(split[1].split(': ')[1][:-1])


def retrieve_textview_files(location='/tmp/textview'):
    textview_files = []
    files = []
    for r, d, f in os.walk(location):
        if r == location:
            textview_files = [x for x in f if os.path.splitext(x)[-1] == '.textview']
            break
    for f in textview_files:
        t = TextViewFile(f, root=location)
        files.append(t)
    return files
