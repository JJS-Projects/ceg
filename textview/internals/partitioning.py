import matplotlib.pyplot as plt
import cv2
import numpy as np
import uuid
import copy
from ..utils.pathfinding import astar
from .constants import Flow

def partition(single, initial, boxes, selected_line, min_contour_size, hierarchy, contours, constraints, elm_collection, draw, width, select, flow, improved):

    original_draw = draw

    request_recalculation = False

    shape = (*(improved.shape), 3)

    imageid = uuid.uuid4().hex

    arranged = [x for x in boxes if x[1] >= selected_line[0] and x[1] + x[3] <= selected_line[1] and x[4] > min_contour_size]
    arranged = [x for x in arranged if hierarchy[0][x[-1]][3] == -1]
    arranged = sorted(arranged, key=lambda x: x[0]) # TODO

    if len(arranged) < 2:
        #continue
        return None, None

    mxU = selected_line[0]
    mxD = selected_line[1]

    current_line = initial[selected_line[0]:selected_line[1], :]
    if draw:
        plt.figure(figsize=(15, 9))
        plt.title(f"CURRENT {imageid}")
        plt.imshow(current_line, cmap='gray')
        plt.show()

    line = []
    for i, y in enumerate(cv2.cvtColor(current_line, cv2.COLOR_RGB2GRAY).T):
        if np.sum(y) > 0:
            line.append(i - 1)
            break

    #################################################################################
    #################################################################################
    #################################################################################

    #########################
    draw = False
    #########################


    last = len(arranged)
    i = 0
    j = 0
    if draw:
        plt.figure(figsize=(50, 20))
        max_line = 10
        line_nums = int(np.ceil((len(arranged) * 2) / max_line))
        internal_count = 1

    x1, y1, w1, h1, _, _ = arranged[i] # first contour
    while True:
        if j >= last:
            break
        x2, y2, w2, h2, _, _ = arranged[j] # second contour
        minx = min(x1, x2)
        maxx = max(x1 + w1, x2 + w2)

        grid = initial[mxU:mxD, minx:maxx] # viewport on both

        grid = cv2.cvtColor(grid, cv2.COLOR_RGB2GRAY)

        if draw:
            plt.subplot(line_nums, max_line, internal_count)
            plt.title(f"ELEMENTS {j-i+1} {imageid}")
            plt.imshow(grid, cmap='gray')
            internal_count += 1

        for x3, col in enumerate(grid.T): # get first empty column
            if np.sum(col) == 0:
                break
        if x3 > 0 and x3 < grid.shape[1] - 1: # do not add it if it's the first or last in viewport
            if draw:
                check = cv2.cvtColor(grid, cv2.COLOR_GRAY2RGB)
                check[:, x3] = [255, 255, 0]
                check[:, x2 - 1 - minx] = [255, 255, 0]
                plt.subplot(line_nums, max_line, internal_count)
                plt.imshow(check, cmap='gray')
                plt.title(f"GOOD L {x3/width > 1.2} : R {(check.shape[1]-(x2-1-minx))/width > 1.2} {imageid}")
                internal_count += 1
            line.append(minx + x3) # add line to x3 from start of viewport (x3 being the first empty column)
            line.append(x2 - 1) # add line right before the second contour

            if (grid.shape[1] - (x2 - 1 - minx)) / width > 1.2:
                for x, E in enumerate(arranged[i:]):
                    if E[0] >= x2 - 1:
                        break
                i = i + x
                x1, y1, w1, h1, _, _ = arranged[i] # first contour after split becomes first, second stays the same ; they can be the same one
            else:
                i = j
                j = i + 1 # mark the second index to be the end of the first contour
                x1, y1, w1, h1, _, _ = arranged[i] # second contour becomes first
        else:
            if draw:
                check = cv2.cvtColor(grid, cv2.COLOR_GRAY2RGB)
                plt.show()
                plt.figure(figsize=(50, 20))
                max_line = 10
                line_nums = int(np.ceil((len(arranged) * 2) / max_line))
                internal_count = 1
            if grid.shape[1] - 1 >= width:
                if draw:
                    i, j, request_recalculation, line = find_spaces(improved, arranged, imageid, grid,mxU,mxD,width,i,j,minx,maxx,draw,line,check=check)
                else:
                    i, j, request_recalculation, line = find_spaces(improved, arranged, imageid, grid,mxU,mxD,width,i,j,minx,maxx,draw,line)
                x1, y1, w1, h1, _, _ = arranged[i] # second contour becomes first
            else: # in case it's not the correct width we add an extra contour to the viewport
                j = j + 1
                x1 = minx
                w1 = maxx - minx
    if draw:
        plt.show()

    if request_recalculation:
        contours, hierarchy = cv2.findContours(improved, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        boxes = []
        for i in range(len(contours)):
            x, y, w, h = cv2.boundingRect(contours[i])
            a = cv2.contourArea(contours[i])
            boxes.append((x, y, w, h, a, i))
        arranged = [x for x in boxes if x[1] >= selected_line[0] and x[1] + x[3] <= selected_line[1] and x[4] > min_contour_size]
        arranged = sorted(arranged, key=lambda x: x[0]) # TODO

    for i, y in enumerate(cv2.cvtColor(current_line, cv2.COLOR_RGB2GRAY).T[::-1]): # add line to the end
        if np.sum(y) > 0:
            line.append(current_line.shape[1] - i)
            break

    ####################
    draw = original_draw
    ####################

    if draw:
        partitioned = np.zeros(shape, dtype=np.uint8)
        for p, (x, y, w, h, a, i) in enumerate(arranged):
            if hierarchy[0][i][3] == -1:
                cv2.drawContours(partitioned, contours, i, (255, 0, 0), -1, hierarchy=hierarchy, maxLevel=2)

        for l in line:
            partitioned[:, l] = [255, 255, 0]

        plt.figure(figsize=(50, 50))
        plt.subplot(141)
        plt.imshow(partitioned)
        plt.title("PARTITIONED")
        plt.subplot(142)
        plt.title("ZOOMED")
        plt.imshow(partitioned[mxU:mxD, line[0]:line[-1]])
        plt.show()
        #return None,None,None,None,None

    #################################################################################################################
    #################################################################################################################
    #################################################################################################################
    #################################################################################################################

    print('LINE DATA', line)
    elm_new = build_elm_new(line, width, selected_line)
    #elm = build_elm(line, width, selected_line)
    elm = elm_new

    print('ELM DATA', elm)
    #################################################################################################################
    #################################################################################################################
    #################################################################################################################
    #################################################################################################################

    if draw:
        selected = np.zeros(shape, dtype=np.uint8)
        for p, (x, y, w, h, a, i) in enumerate(arranged):
            if hierarchy[0][i][3] == -1:
                cv2.drawContours(selected, contours, i, (255, 0, 0), -1, hierarchy=hierarchy, maxLevel=2)

        for l in elm:
            selected[:, l[0]] = [0, 0, 255]
            selected[:, l[1]] = [255, 0, 0]
        plt.figure(figsize=(50, 50))
        plt.subplot(141)
        plt.imshow(selected, cmap='gray')
        plt.title("SELECTED")
        plt.subplot(142)
        plt.title("ZOOMED")
        plt.imshow(selected[mxU:mxD, line[0]:line[-1]])
        plt.show()

    save_index = [i for i, x in enumerate(elm) if x[0] <= select[0] <= x[1]]
    if not save_index and single:
        distance = shape[1]
        for i, x in enumerate(elm):
            if abs(select[0] - x[0]) < distance:
                distance = abs(select[0] - x[0])
                save_index = i
            if abs(select[0] - x[1]) < distance:
                distance = abs(select[0] - x[1])
                save_index = i
    elif save_index:
        save_index = save_index[0]
    else:
        save_index = 0

    if draw:
        factor_blue = 128
        factor_red = 128

        blue = 255
        red = 255

        segmented = np.zeros(shape, dtype=np.uint8)

        for p, e in enumerate(elm):
            blue -= factor_blue
            blue = 255 if blue < 0 else blue
            red -= factor_red
            red = 255 if red < 0 else red
            for x, y, w, h, _, i in arranged:
                if e[0] < x + (w // 2) < e[1]:
                    if hierarchy[0][i][3] == -1:
                        if p < save_index:
                            cv2.drawContours(segmented, contours, i, (0, 0, blue), -1, hierarchy=hierarchy, maxLevel=2)
                            segmented = cv2.putText(segmented, str(p), (x, y + h), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [255, 255, 0])
                        elif p > save_index:
                            cv2.drawContours(segmented, contours, i, (red, 0, 0), -1, hierarchy=hierarchy, maxLevel=2)
                            segmented = cv2.putText(segmented, str(p), (x, y + h), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [255, 255, 0])
                        else:
                            cv2.drawContours(segmented, contours, i, (0, 255, 0), -1, hierarchy=hierarchy, maxLevel=2)
                            segmented = cv2.putText(segmented, str(p), (x, y + h), cv2.FONT_HERSHEY_SIMPLEX, 0.4, [255, 255, 0])
                elif x + w // 2 > e[1]:
                    break

        plt.figure(figsize=(50, 50))
        plt.subplot(141)
        plt.imshow(segmented, cmap='gray')
        plt.title("SEGMENTED")
        plt.subplot(142)
        plt.title("ZOOMED")
        plt.imshow(segmented[mxU:mxD, line[0]:line[-1]])
        plt.show()

    print("Number of elements: ", len(elm))
    print("Selected element index: ", save_index)

    if len(elm) < 2:
        return None,None

    if flow == Flow.VERTICAL:
        draw_on = np.zeros_like(improved)
        for idx, e in enumerate(elm):
            min_x = improved.shape[1]
            min_y = improved.shape[0]
            max_x = 0
            max_y = 0
            for x, y, w, h, _, i in arranged:
                if e[0] < x + (w // 2) < e[1] and e[2] < y + (h // 2) < e[3]:
                    if hierarchy[0][i][3] == -1:
                        cv2.drawContours(draw_on, contours, i, 255, -1, hierarchy=hierarchy, maxLevel=2)#,offset=tuple(offset))
                        min_x = min(min_x, x)
                        min_y = min(min_y, y)
                        max_x = max(max_x, x + w)
                        max_y = max(max_y, y + h)

            y_f_s = improved.shape[0] - e[3]
            y_f_e = improved.shape[0] - e[2]
            constraints.append((y_f_s, min_x, y_f_e - y_f_s, max_x - min_x))

            if (max_y - min_y) / (max_x - min_x) < 0.25:
                print('INDEX', idx, 'CHOUON')
            else:
                elm[idx] = (elm[idx][0], elm[idx][1], elm[idx][2], elm[idx][3], True)
    else:
        blank = np.zeros_like(improved)
        for p, e in enumerate(elm):
            min_x = improved.shape[1]
            min_y = improved.shape[0]
            max_x = 0
            max_y = 0
            for x, y, w, h, _, i in arranged:
                if e[0] < x + (w // 2) < e[1] and e[2] < y + (h // 2) < e[3]:
                    if hierarchy[0][i][3] == -1:
                        cv2.drawContours(blank, contours, i, 255, -1, hierarchy=hierarchy, maxLevel=2)#,offset=(p*spacing*-1,0))
                        min_x = min(min_x, x)
                        max_x = max(max_x, x + w)
                elif x + w // 2 > e[1]:
                    break

            constraints.append((min_x, e[2], max_x - min_x, e[3] - e[2]))
    elm_collection.append(elm)

    return elm_collection, constraints


def find_spaces(improved, arranged, imageid, grid, mxU, mxD, width, i, j, minx, maxx, draw, line, check=None):
    request_recalculation = False
    inspect = width

    def calculate_cost(ans, grid):
        cost = 0
        for elm in ans:
            if grid[elm[1], elm[0]] != 0: # if it's not white e.g. >= 127
                cost += 1
        return cost

    quick_fix = False
    if grid.shape[1] < width * 1.1:
        quick_fix = True
    min_inspect = grid.shape[1] // 2
    if draw:
        check[:, inspect] = [0, 0, 255]
        plt.figure(figsize=(50//4, 20//4))
        plt.title(f'CHECK {imageid}')
        plt.imshow(check, cmap='gray')
        plt.show()
    best_path = None

    step = abs(inspect - min_inspect) // 5
    step = max(step, 1)

    if inspect < min_inspect:
        j = j + 1

    while inspect >= min_inspect:
        print('PATHFINDING', minx, maxx, inspect, min_inspect, step, True)
        if grid[0, inspect] == 0 and grid[grid.shape[0] - 1, inspect] == 0:
            ans = astar(inspect, 0, inspect, grid.shape[0] - 1, grid)
            if best_path is None:
                best_path = ans
                inspect -= step
                if quick_fix:
                    inspect = min_inspect - 1
            elif len(ans) < len(best_path):
                best_path = ans
                inspect -= step
            else:
                inspect = min_inspect - 1
        else:
            inspect -= step

        if inspect < min_inspect:
            print('TERMINATE')
            if best_path is not None:
                ans = best_path
                if calculate_cost(ans, grid) > 0:
                    print('RECALCULATE')
                    for elm in ans:
                        grid[elm[1], elm[0]] = 0
                    improved[mxU:mxD, minx:maxx] = grid # Clear path on original and request contour recalculation
                    request_recalculation = True #
            else:
                ans = []
            if len(ans) > 0:
                if draw:
                    for elm in ans:
                        grid[elm[1], elm[0]] = 127

                    plt.figure(figsize=(50//4, 20//4))
                    plt.title(f'Best path {imageid}')
                    plt.imshow(grid, cmap="gray")
                    plt.show()
                left_x = grid.shape[1]
                right_x = 0
                for x0, _ in ans:
                    if x0 < left_x:
                        left_x = x0
                    if x0 > right_x:
                        right_x = x0
                middle = minx + left_x + (right_x - left_x) // 2
                line.append(middle)
                line.append(middle)
                i = j
                j = i + 1
                x1, y1, w1, h1, _, _ = arranged[i]
            else:
                middle = minx + min_inspect
                line.append(middle)
                line.append(middle)
                if draw:
                    check[:, inspect] = [255, 0, 0]
                    plt.figure(figsize=(50//4, 20//4))
                    plt.title(f'Warning! Split in half {imageid}')
                    plt.imshow(check, cmap="gray")
                    plt.show()

                i = j
                j = i + 1
                x1, y1, w1, h1, _, _ = arranged[i]
            break
    return i, j, request_recalculation, line

class DinElm(object):
    def __init__(self,start,end):
        self.space_start = -1
        self.start = start
        self.end = end
        self.size = end - start
        self.space_end = -1
        self.index = 0
        self.group = -1

    def __str__(self):
        return f"\x1b[1;34m{self.index}\x1b[0m,{self.space_start}:{self.size}:{self.space_end}"
    def __repl__(self):
        return f"\x1b[1;34m{self.index}\x1b[0m,{self.space_start}:{self.size}:{self.space_end}"


class DinGroup(object):
    def __init__(self,width):
        self.group = []
        self.size = -1
        self.start = -1
        self.end = -1
        self.space_start = -1
        self.space_end = -1
        self.width = width
        self.current = 1000.0
        self.count = 0
        self.intermax = 0

    #def insert(self, elm):
    #    tmp = self.group
    #    self.group = [elm]
    #    self.group.extend(tmp)
    #    self.update()

    def update(self):
        self.start = self.group[0].start
        self.end = self.group[-1].end
        self.space_start = self.group[0].space_start
        self.space_end = self.group[-1].space_end
        self.size = self.end - self.start
        self.current = abs(100.0-(self.size/(self.width/100)))

    def add(self, elm):
        if not self.group:
            self.group.append(elm)
            self.count += 1
            self.update()
            return True
        else:
            intermax = max(self.intermax, elm.space_start)
            #if (self.space_start == -1 and intermax <= elm.space_end) or (self.space_start >= intermax <= elm.space_end):
            self.intermax = intermax
            self.group.append(elm)
            #self.group = sorted(self.group,key=lambda x:x.start)
            self.count += 1
            self.update()
            return True
            #else:
            #    return False

    def remove(self, index=-1):
        item = self.group[index]
        del self.group[index]
        #self.group = sorted(self.group,key=lambda x:x.start)
        self.count -= 1
        if self.group:
            self.update()
            self.intermax = 0
            for i in self.group[:-1]:
                self.intermax = max(self.intermax, i.space_end)
        else:
            self.size = -1
            self.start = -1
            self.end = -1
            self.space_start = -1
            self.space_end = -1
            self.current = 1000.0
            self.intermax = 0
        return item

    def __str__(self):
        return f"\x1b[1;31m{self.current}\x1b[0m,{self.space_start}:{self.size}:{self.space_end}"

    def __repl__(self):
        return f"\x1b[1;31m{self.current}\x1b[0m,{self.space_start}:{self.size}:{self.space_end}"

class DinBuilder(object):
    def __init__(self, width=-1, spacing=-1):
        self.width = width
        self.spacing = spacing
        self.top = []
        self.bottom = []
        self.swap_count = 0

    def insert(self,item):
        if self.bottom:
            prev = self.bottom[-1]
            prev.space_end = item.start - prev.end
            item.space_start = prev.space_end
            item.index = prev.index + 1
        self.bottom.append(item)

    def build(self):
        group = DinGroup(self.width)
        self.top.append(group)
        for idx, i in enumerate(self.bottom):
            group_before = copy.deepcopy(self.top[-1])
            prev  = self.top[-1].current

            status = self.top[-1].add(i)
            #if not status:
            #    group = DinGroup(self.width)
            #    self.top.append(group)
            #    status = self.top[-1].add(i)
            #else:
            after = self.top[-1].current
            if after > prev:
                group_after = copy.deepcopy(self.top[-1])
                removed = group_after.remove(0)
                if group_before.current < group_after.current or group_before.intermax < group_after.intermax:
                    self.top[-1] = group_before
                    group = DinGroup(self.width)
                    group.add(i)
                    self.top.append(group)
                else:
                    group = DinGroup(self.width)
                    group.add(removed)
                    self.top[-1] = group
                    self.top.append(group_after)
            else:
                group = DinGroup(self.width)
                group.add(i)
                if self.top[-1].current > group.current:# or (self.top[-1].space_start/2 < self.top[-1].intermax > self.top[-1].space_end/2):
                    self.top[-1] = group_before
                    self.top.append(group)

    def display(self):
        print('{')
        for i in self.bottom:
            print('\t',str(i),',')
        print('}')

    def swap(self):
        tmp = self.top
        self.top = self.bottom
        self.bottom = tmp
        self.swap_count += 1

    def collapse(self, defined_top=-1, defined_bottom=-1):
        while self.swap_count % 2:
            self.swap()
            self.swap_count -= 2
        elm = []
        for e in self.top:
            elm.append((e.start, e.end, defined_top, defined_bottom, False))
        return elm


def build_elm_new(line, width, selected_line):
    builder = DinBuilder(width=width)
    for i in range(len(line)):
        if i % 2 == 0:
            builder.insert(DinElm(line[i],line[i+1]))
    builder.display()
    builder.build()
    builder.swap()
    builder.display()
    return builder.collapse(defined_top=selected_line[0], defined_bottom=selected_line[1])


def build_elm(line,width,selected_line):
    i = 0
    j = 1
    elm = []
    last = len(line)
    while True:
        if j >= last:
            break
        print('i', i, 'j', j, 'width', line[j] - line[i])
        if line[j] - line[i] > width:
            if j - i != 1:
                if j - i < 5:
                    if line[j] - line[i] > width * 1.2: # FIXME this is quite arbitrary
                        if len(elm) > 0:
                            tmp = elm[-1]
                            del elm[-1]
                            start = tmp[0]
                            end = tmp[1]
                            x = j - 4
                            best = line[j - 2] - start
                            idx = j - 2
                            while x > 0 and line[x] > start:
                                d = abs((line[x] - start) - (line[j - 2] - line[x + 1]))
                                if d < best:
                                    best = d
                                    idx = x
                                x -= 2

                            elm.append((tmp[0], line[idx], tmp[2], tmp[3], tmp[4]))
                            elm.append((line[idx + 1], line[j - 2], tmp[2], tmp[3], tmp[4]))
                            i = j - 1
                        else:
                            if line[i] < line[j - 2]:
                                elm.append((line[i], line[j - 2], selected_line[0], selected_line[1], False))
                                i = j - 1
                    else:
                        elm.append((line[i], line[j], selected_line[0], selected_line[1], False))
                        i = j + 1
                        j = i + 1
                else:
                    inter_spacing = line[j - 3] - line[j - 4]
                    spacing = line[j - 1] - line[j - 2]
                    if inter_spacing > spacing:
                        if line[j - 4] - line[i] < 0.7 * width:
                            elm.append((line[i], line[j - 2], selected_line[0], selected_line[1], False))
                            i = j - 1

                        else:
                            elm.append((line[i], line[j - 4], selected_line[0], selected_line[1], False))
                            i = j - 3
                            j = i + 1

                    else:
                        elm.append((line[i], line[j - 2], selected_line[0], selected_line[1], False))
                        i = j - 1

            else:
                print('Error', i, j)
                elm.append((line[i], line[j], selected_line[0], selected_line[1], False))
                i = j + 1
                j = i + 1
        else:
            j = j + 2
    if (j - 2) > i:
        elm.append((line[i], line[j - 2], selected_line[0], selected_line[1], False))
    return elm
