import numpy as np


def box_point_test(Q, select):
    (x, y, w, h) = Q
    if x <= select[0] <= x + w and y <= select[1] <= y + h:
        return True
    else:
        return False


def box_relation(X, Y):
    (x1, y1, w1, h1) = X
    (x2, y2, w2, h2) = Y
    dist_x = min(abs(x1 - (x2 + w2)), abs(x2 - (x1 + w1)))
    dist_y = min(abs(y1 - (y2 + h2)), abs(y2 - (y1 + h1)))
    min_x = min(x1, x2)
    max_x = max(x1 + w1, x2 + w2)
    min_y = min(y1, y2)
    max_y = max(y1 + h1, y2 + h2)
    if max_x - min_x > w1 + w2:
        if max_y - min_y > h1 + h2:
            dist = np.sqrt(dist_x**2 + dist_y**2)
        else:
            dist = dist_x
    elif max_y - min_y > h1 + h2:
        dist = dist_y
    else:
        dist = 0 #Relation.INTERSECT
    A = (min_x, min_y, max_x - min_x, max_y - min_y)
    return dist, A


def box_area(X):
    (x1, y1, w1, h1) = X
    return w1 * h1


def return_center(X):
    (x1, y1, w1, h1) = X
    center = (x1 + w1 // 2, y1 + h1 // 2)
    return center


def merge(A, elements, dist=0):
    threshold = 7
    trigg = 0
    count = 0
    for x2, y2, w2, h2, ii, e2 in elements:
        B = (x2, y2, w2, h2)
        if A == B:
            continue
        d, C = box_relation(A, B)
        if d <= dist:
            A = C
            count += 1
        else:
            trigg += 1
        if trigg == threshold:
            break
    return count, A


def proximity_merge(A, elements, distance, skip=0):
    count = 0
    Q = A
    wait = skip
    for x2, y2, w2, h2, ii, e2 in elements:
        B = (x2, y2, w2, h2)
        if A == B:
            continue
        d, _ = box_relation(A, B)
        if d <= distance:
            if wait == 0 or d == 0:
                _, Q = box_relation(Q, B)
                count += 1
            else:
                wait -= 1
        else:
            break
    return count, Q


def return_box(size, center):
    half = size // 2
    x, y = center
    return (x - half, y - half, size, size)
