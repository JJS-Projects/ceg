import cv2
import numpy as np
import matplotlib.pyplot as plt
from .constants import Flow
from .partitioning import partition
#from .relations import box_relation


def segment(improved, select, min_contour_size, flow=Flow.HORIZONTAL, single=False, expected=None, draw=False):
    # TODO : if flow is vertical perform a rotation operation with M = cv2.getRotationMatrix2D(center, 90, 1) using cv2.warpAffine() on every character
    #draw = True
    #global available_outside
    original_draw = draw

    if flow not in [Flow.HORIZONTAL, Flow.VERTICAL]:
        return improved.shape, [], [], [], []

    print('SELECT', select)

    #print(improved.dtype)

    #ret, markers = cv2.connectedComponents(improved)
    #markers = markers+1
    #markers[both==255] = 0
    #plt.figure(figsize=(15,9))
    #plt.imshow(markers,cmap='gray')
    #plt.title("INITIAL")
    #plt.show()

    #improved[60:,:] = 0#[0,0,0] # minor hack / removing lines

    request_recalculation = False
    contours, hierarchy = cv2.findContours(improved, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    boxes = []
    shape = (*(improved.shape), 3)
    initial = np.zeros(shape, dtype=np.uint8)
    #print('blank shape',blank.shape)
    for i in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[i])
        a = cv2.contourArea(contours[i])
        boxes.append((x, y, w, h, a, i))
        if a > min_contour_size:
            if hierarchy[0][i][3] == -1:
                cv2.drawContours(initial, contours, i, (255, 0, 0), -1, hierarchy=hierarchy, maxLevel=2)

    if expected is None:
        sorted_boxes = sorted(boxes, key=lambda x: x[2], reverse=True)
        try:
            calculated_width = next((x[2] for x in sorted_boxes if x[3] / x[2] > 0.5 and x[3] / x[2] < 2))
        except StopIteration:
            return improved.shape, [], [], [], []

        width = 20
        height = 20
        if calculated_width > width:
            width = calculated_width
    else:
        if flow == Flow.HORIZONTAL:
            width = expected[2]
            height = expected[3]
        elif flow == Flow.VERTICAL:
            width = expected[3]
            height = expected[2]

    print('HEIGHT', height)
    print('WIDTH', width)

    # TODO : Draw selected element
    #if draw:
    #    height = 0
    #    M = None
    #    #for idx,(x1,y1,w1,h1,_,_) in enumerate(boxes):
    #    #    for x2,y2,w2,h2,_,_ in boxes[idx:]:
    #    #        d, C = box_relation((x1,y1,w1,h1),(x2,y2,w2,h2))
    #    #        if d == 0:
    #    #            if 0.9 < C[3]/C[2] < 1.3:
    #    #                old_width = width
    #    #                old_height = height
    #    #                width = max(width,C[2])
    #    #                height = max(height,C[3])
    #    #                if old_width != width or old_height != height:
    #    #                    M = C
    #    tmp = np.zeros_like(improved)
    #    if M is not None:
    #        cv2.rectangle(tmp, (M[0], M[1]), (M[0] + M[2], M[1] + M[3]), 255, 3)
    #    else:
    #        M = sorted_boxes[0][:4]
    #        cv2.rectangle(tmp, (M[0], M[1]), (M[0] + M[2], M[1] + M[3]), 255, 3)
    #    plt.imshow(tmp, cmap='gray')
    #    plt.show()

    #    height += 2 # one pixel larger on all sides
    #    width += 2

    #    print('WIDTH', width)
    #    print('HEIGHT', height)

    #sorted_y = sorted(boxes,key=lambda x:x[3],reverse=True)[0][3]
    #arranged = sorted(boxes,key=lambda x:x[0]) # TODO

    #blank[0:25,:] = [0,0,0] # minor hack / removing top lines
    #blank[60:,:] = [0,0,0] # minor hack / removing lines

    # TESTING
    #plt.figure(figsize=(15,9))
    #plt.imshow(blank,cmap='gray')
    #plt.title("Contour drawn")
    #plt.show()

    lines = []
    line = []
    inside = False

    for i, y in enumerate(initial):
        count = np.sum(y)
        if count > 0 and not inside:
            if i != 0:
                #line.append(i-1)
                line.append(i)
            else:
                line.append(0)
            inside = True
        elif inside and count == 0:
            inside = False
            line.append(i)
            lines.append(tuple(line))
            line = []
            inside = False

    remove = []
    for i in range(len(lines)):
        start, end = lines[i]
        if draw:
            line = initial[start:end, :]
            plt.figure(figsize=(15, 9))
            plt.imshow(line, cmap='gray')
            plt.title("LINE " + str(i + 1) + " size " + str(end - start))
            plt.show()
        if end - start < max(20, (height + 2) // 2): # if the line is half the required
            remove.append(i)

    if remove:
        for x in sorted(remove, reverse=True):
            del lines[x]

    if single:
        try:
            lines = [x for x in lines if x[0] <= select[1] <= x[1]]
        except IndexError as e:
            print(e)
            print('The selected point is outside the viewable area')
            return None, None, None, None

    if single and draw:
        plt.figure(figsize=(15, 9))
        plt.imshow(initial[lines[0][0]:lines[0][1], :], cmap='gray')
        plt.title("ON LINE")
        plt.show()

    elm_collection = []
    constraints = []

    for selected_line in lines:

        elm_collection, constraints = partition(single, initial, boxes, selected_line, min_contour_size, hierarchy, contours, constraints, elm_collection, draw, width, select, flow, improved)

    elm = []
    for e in elm_collection:
        elm.extend(e)

    #cv2.imwrite('save_image.png',padded)

    return improved.shape, elm, constraints, contours, hierarchy

def splitLines(elm):
    line = []
    lines = []
    if not elm:
        return lines
    const = elm[0][2:4]
    for e in elm:
        if e[2] == const[0] and e[3] == const[1]:
            line.append(e)
        else:
            lines.append(line)
            const = e[2:4]
            line = [e]
    lines.append(line)
    return lines
