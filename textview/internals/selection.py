import cv2
import os
import uuid
#from importlib import reload
from .extraction import extraction, single_shot
import time
from .constants import Color, Flow
from .drawing import returnCEGindex
from .relations import box_relation
import numpy as np
from .helper import is_grayscale, interpolate, rescale_select, contour_removal

savelocation = '/tmp/textview'


def set_savelocation(location):
    global savelocation
    savelocation = location


def obtain():
    import subprocess
    import re
    import shlex

    try:
        os.mkdir(savelocation)
    except FileExistsError:
        pass

    res = subprocess.check_output(shlex.split("xdotool getmouselocation"))
    res = str(res, 'utf-8')[:-1] # remove \n
    res = res.split()
    x = res[0].split(":")[1]
    y = res[1].split(":")[1]
    w = res[3].split(":")[1]
    res = str(subprocess.check_output(shlex.split(f"xdotool getwindowgeometry {w}")), 'utf-8')[:-1]
    pos = re.match(r".*Position: (.*?) \(", res, re.DOTALL).group(1)
    pos = pos.split(',')
    x = int(x) - int(pos[0])
    y = int(y) - int(pos[1])

    fileid = uuid.uuid4().hex
    imagepath = os.path.join(savelocation, f'{fileid}.bmp')
    print('imagepath', imagepath)
    res = subprocess.check_call(shlex.split(f"import -window {w} {imagepath}"))

    if res == 0:
        return (imagepath, (x, y))
    else:
        return (None, None)


def constraints_refit(constraints, text):
    # FIXME remove overlapping
    new_constraints = []
    cur = 0
    for word in text.words:
        #print(word, '→', word.dict, '▷', word.reading)
        length = len(word)
        X = constraints[cur]
        for G in constraints[cur:cur + length]:
            d, X = box_relation(X, G)
        G = X
        new_constraints.append(G)
        #if cur <= index < cur + length:
        #    cv2.rectangle(copy, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.BLUE, 3)
        #else:
        #    cv2.rectangle(copy, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.YELLOW, 3)
        cur = cur + length
    return new_constraints


complete_full = np.zeros((50, 50), np.uint8)


def interact():
    global complete_full

    try:
        import jishox
        import jishox.frontend.client
        settings = jishox.frontend.client.reload_config()
    except Exception:
        pass

    set_savelocation('./tmp')
    image = np.zeros((50, 50), np.uint8)
    cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
    flow = Flow.INVALID
    select = (0, 0)
    all_constraints = []
    all_text = []

    show_full = False

    def draw(image, select, lang='jpn', terminate=False):
        global complete_full
        trigger = True
        copy = image.copy()
        for i, constraints in enumerate(all_constraints):
            index = returnCEGindex(constraints, select)
            if index != -1:
                trigger = False
                text = all_text[i]
                check = text.words[index].dict
                if not check:
                    check = str(text.words[index])
                print('INDEX', index, check)
                try:
                    jishox.frontend.client.send(settings, check, result_lang=lang)
                except Exception:
                    pass
            for idx, G in enumerate(constraints):
                if idx == index:
                    cv2.rectangle(copy, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.BLUE, 3)
                else:
                    cv2.rectangle(copy, (G[0], G[1]), (G[0] + G[2], G[1] + G[3]), Color.YELLOW, 3)
        imgo = cv2.cvtColor(copy, cv2.COLOR_RGB2BGR)
        cv2.imshow('Image', imgo)
        if trigger and not terminate:
            print('ADDING')
            start_time = time.time()
            grayscale = is_grayscale(image)
            #image = interpolate(image)
            #old_select = select
            #select = rescale_select(select)
            if grayscale:
                print('IMAGE IS GRAYSCALE')
                improved = extraction(image, select, sharp=True, erode=0)
            else:
                improved = extraction(image, select, sharp=False, erode=0)
            print(improved.collapse())
            print()
            print("#" * 40)
            print('DONE')
            print("#" * 40)
            print("--- %s seconds ---" % (time.time() - start_time))
            try:
                constraints = improved.constraints
                full, select, flow = improved.extra
                #text = ''.join([x['literal'] if x['conf'] > 80 else '◯' for x in improved.collapse()])
                text = ''.join([x['literal'] for x in improved.collapse() if x])
                complete_full = cv2.bitwise_or(complete_full, full)
                print(text)
                full_fileid = uuid.uuid4().hex
                cv2.imwrite(os.path.join(savelocation, full_fileid + '.jpg'), full)
                with open(os.path.join(savelocation, f'{fileid}.textview'), 'a') as fd:
                    fd.write(f'{"#"*32} -:- SELECT: {select}\n')
                    fd.write(f'{full_fileid} -:- {improved.collapse()}\n')
                try:
                    text = jishox.Text(text)
                    constraints = constraints_refit(constraints, text)
                    all_constraints.append(constraints)
                    all_text.append(text)
                except Exception:
                    pass
                draw(image, select, terminate=True)
            except (IndexError, AttributeError, TypeError):
                pass

    def select_point(event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            print(f'\x1b[1;34mLOC {(x, y)}\x1b[0m')
            select = (x, y)
            draw(image, select, lang='jpn')
        if event == cv2.EVENT_MBUTTONDOWN:
            print(f'\x1b[1;34mLOC {(x, y)}\x1b[0m')
            select = (x, y)
            draw(image, select, lang='eng')

    cv2.setMouseCallback('Image', select_point)
    cv2.imshow('Image', image)
    fileid = ""

    while True:

        k = cv2.waitKey()

        if k & 0xFF == 27:
            break
        elif k & 0xFF == ord('s') and show_full:
            tmp, select = contour_removal(complete_full)
            if np.all(tmp == complete_full):
                cv2.imshow('Image', tmp)
            else:
                all_constraints = []
                improved = single_shot(image, tmp, select)
                try:
                    constraints = improved.constraints
                    full, select, flow = improved.extra
                    #text = ''.join([x['literal'] if x['conf'] > 80 else '◯' for x in improved.collapse()])
                    text = ''.join([x['literal'] for x in improved.collapse() if x])
                    print(text)
                    complete_full = full
                    full_fileid = uuid.uuid4().hex
                    cv2.imwrite(os.path.join(savelocation, full_fileid + '.jpg'), full)
                    with open(os.path.join(savelocation, f'{fileid}.textview'), 'a') as fd:
                        fd.write(f'{"#"*32} -:- SELECT: {select}\n')
                        fd.write(f'{full_fileid} -:- {improved.collapse()}\n')
                    try:
                        text = jishox.Text(text)
                        constraints = constraints_refit(constraints, text)
                        all_constraints.append(constraints)
                        all_text.append(text)
                    except Exception:
                        pass
                    draw(image, select)
                except (IndexError, AttributeError, TypeError) as e:
                    print(f'\x1b[1;31mERROR {e}\x1b[0m')
                    image = np.zeros((50, 50, 3), np.uint8)
                    image[:, :, :] = Color.RED
                    imgo = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                    cv2.imshow('Image', imgo)
                    pass
        elif k & 0xFF == ord('r'):
            if not show_full:
                if flow == Flow.VERTICAL:
                    tmp = cv2.rotate(complete_full, cv2.ROTATE_90_CLOCKWISE)
                if flow == Flow.HORIZONTAL:
                    tmp = complete_full
                if flow in [Flow.HORIZONTAL, Flow.VERTICAL]:
                    cv2.imshow('Image', tmp)
                    show_full = True
            else:
                show_full = False
                imgo = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                cv2.imshow('Image', imgo)
        elif k & 0xFF == ord('q'):
            all_constraints = []
            imagepath, select = obtain()
            image = cv2.imread(imagepath)
            fileid = os.path.basename(imagepath)
            fileid = os.path.splitext(fileid)[0]
            print(f'\x1b[1;34mIMAGE {fileid}\x1b[0m')

            with open(os.path.join(savelocation, f'{fileid}.textview'), 'a') as fd:
                fd.write(f'{fileid} -:- SELECT: {select}\n')

            cv2.imshow('Image', image)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            start_time = time.time()
            grayscale = is_grayscale(image)
            image = interpolate(image)
            select = rescale_select(select)
            if grayscale:
                print('IMAGE IS GRAYSCALE')
                improved = extraction(image, select, sharp=True, erode=0)
            else:
                improved = extraction(image, select, sharp=False, erode=0)
            print(improved.collapse())
            print()
            print("#" * 40)
            print('DONE')
            print("#" * 40)
            print("--- %s seconds ---" % (time.time() - start_time))
            try:
                constraints = improved.constraints
                full, select, flow = improved.extra
                #text = ''.join([x['literal'] if x['conf'] > 80 else '◯' for x in improved.collapse()])
                text = ''.join([x['literal'] for x in improved.collapse() if x])
                print(text)
                complete_full = full
                full_fileid = uuid.uuid4().hex
                cv2.imwrite(os.path.join(savelocation, full_fileid + '.jpg'), full)
                with open(os.path.join(savelocation, f'{fileid}.textview'), 'a') as fd:
                    fd.write(f'{full_fileid} -:- {improved.collapse()}\n')
                try:
                    text = jishox.Text(text)
                    constraints = constraints_refit(constraints, text)
                    all_constraints.append(constraints)
                    all_text.append(text)
                except Exception:
                    pass
                draw(image, select)
            except (IndexError, AttributeError, TypeError) as e:
                print(f'\x1b[1;31mERROR {e}\x1b[0m')
                image = np.zeros((50, 50, 3), np.uint8)
                image[:, :, :] = Color.RED
                imgo = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                cv2.imshow('Image', imgo)
                pass

    cv2.destroyAllWindows()
    #return store.points
