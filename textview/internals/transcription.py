from .segmentation import splitLines
from .drawing import drawCEGimage, returnCEGindex
import cv2
import os


def call_tesseract(elm, T=[7, 13], filename='save_image'):
    import xml.etree.ElementTree as ET
    from xml.etree.ElementTree import ParseError
    import subprocess
    from subprocess import CalledProcessError
    import os
    #T = [1,3,7,13]
    #T = [7,13]
    datapoints = []

    try:
        print(f'\x1b[1;33mTESS EXECUTE {filename}\x1b[0m')
        _ = subprocess.run(f"convert {filename}.png {filename}.tif", shell=True)
    except CalledProcessError as e:
        print(f'\x1b[1;31mTESS SUBPROCESS CONVERSION ERROR {e}\x1b[0m')
        return []

    for X in T:
        try:
            _ = subprocess.run(f"tesseract {filename}.tif - -l jpn --psm {X} -c lstm_choice_mode=2 hocr > {filename}.hocr", shell=True)
            # FIX errors in tesseract xml creation
            command = r"sed -i -e 's/>&</>\&amp;</g'"
            command += f" {filename}.hocr"
            _ = subprocess.run(command, shell=True) # add more as needed
        except CalledProcessError as e:
            print(f'\x1b[1;31mTESS SUBPROCESS ERROR {e}\x1b[0m')
            return []
        try:
            ns = {'xhtml': 'http://www.w3.org/1999/xhtml'}
            tree = ET.parse(f'{filename}.hocr')
            root = tree.getroot()
            line = root.find('xhtml:body/xhtml:div/xhtml:div/xhtml:p/xhtml:span', ns) # first line (the only line)
            info = []
            for idx, char in enumerate(line.findall("xhtml:span[@class='ocrx_word']", ns)):
                literal = char.text.strip()
                if len(literal) == 1:
                    info.append({'literal': literal, 'conf': int(char.get('title').split("x_wconf ")[1])})
            datapoints.append(info)
        except AttributeError as e:
            print(f'\x1b[1;31mTESS ATTRIUBUTE ERROR {e}\x1b[0m')
            os.remove(f'{filename}.hocr')
            continue
        except ParseError as e:
            print(f'\x1b[1;31mTESS PARSE ERROR {e}\x1b[0m')
            return []

    try:
        os.remove(f'{filename}.tif')
        os.remove(f'{filename}.hocr')
    except OSError as e:
        print(f'\x1b[1;31mTESS OS ERROR {e}\x1b[0m')
        pass
    return datapoints

    ##from importlib import reload
    ##import jishox
    ##reload(jishox)
    #import xml.etree.ElementTree as ET
    ##from xml.etree.ElementTree import ParseError
    #import subprocess
    #import os
    ##T = [1,3,7,13]
    ##T = [7,13]
    #datapoints = []
    #maxhits = 0
    #for X in T:
    #    try:
    #        print(f'\x1b[1;33mTESS EXECUTE {filename}\x1b[0m')
    #        #_ = subprocess.check_output(f"convert {filename}.png {filename}.tif && tesseract {filename}.tif - -l jpn --psm {X} -c lstm_choice_mode=2 hocr > {filename}.hocr", shell=True)
    #        p = subprocess.run(f"convert {filename}.png {filename}.tif", shell=True)
    #        print('convert', p.returncode)
    #        p = subprocess.run(f"tesseract {filename}.tif - -l jpn --psm {X} -c lstm_choice_mode=2 hocr > {filename}.hocr", shell=True)
    #        print('transcribe', p.returncode)
    #        #try:
    #        #    os.remove(f'{filename}.tif')
    #        #except OSError:
    #        #    print(f'\x1b[1;31mTESS OS ERROR\x1b[0m')
    #        #    pass
    #    except subprocess.CalledProcessError:
    #        print(f'\x1b[1;31mTESS SUBPROCESS ERROR\x1b[0m')
    #        return datapoints
    #    try:
    #        ns = {'xhtml': 'http://www.w3.org/1999/xhtml'} # add more as needed
    #        tree = ET.parse(f'{filename}.hocr')
    #        root = tree.getroot()
    #        line = root.find('xhtml:body/xhtml:div/xhtml:div/xhtml:p/xhtml:span', ns) # first line (the only line)
    #        info = []
    #        for idx, char in enumerate(line.findall("xhtml:span[@class='ocrx_word']", ns)):
    #            literal = char.text.strip()
    #            if len(literal) == 1:
    #                info.append({'literal': literal, 'conf': int(char.get('title').split("x_wconf ")[1])})
    #            #else:
    #            #    info.append({'literal':None,'conf':None})
    #        #print(info)
    #        #if "〇" in text:
    #        #    pass
    #        #else:
    #        #    raise # stop here if tesseract knows what is written in full
    #        datapoints.append(info)
    #        maxhits = max(maxhits, len(info))
    #        #if len(elm) != len(text):
    #        #    print("Incorrect number of characters detected!")
    #        #    raise AttributeError("Incorrect number of char")
    #        #else:
    #        #    datapoints.append(info)
    #        #count = text.count('〇')
    #        #print(f"↑ {count} unknown chars")
    #    except AttributeError:
    #        print(f'\x1b[1;31mTESS ATTRIUBUTE ERROR\x1b[0m')
    #        os.remove(f'{filename}.hocr')
    #        continue
    #    #except:
    #    #    os.remove(f'{filename}.hocr')
    #    #    continue

    #    #except ParseError:
    #    #    print(f'\x1b[1;31mTESS PARSE ERROR\x1b[0m')
    #    #    try:
    #    #        os.remove(f'{filename}.hocr')
    #    #    except OSError:
    #    #        pass
    #    #    continue

    #    os.remove(f'{filename}.hocr')
    #return datapoints
#datapoints = call_tesseract([None]*24)
#other = call_tesseract([None]*24,T=[10])


#%%timeit
#import time
#start_time = time.time()

class Aggregator(object):
    def __init__(self, size):
        if type(size) is int:
            self.collection = {f'{e}': [] for e in range(0, size)}
        elif type(size) is Aggregator:
            self.collection = size.complete()
        print('Aggregator init', self.collection)
        self._extra = None
        self._constraints = None
        self.halt = False
        self.title = ''
        self.collapsed = None

    def insert(self, idx, elem):
        maxsize = len(self.collection)
        for i, elm in enumerate(elem):
            if idx + i < maxsize:
                self.collection[str(idx + i)].append(elm)
        #for i,elm in enumerate(elem1):
        #    self.collection[str(idx+i)].append(elm)
        #print(self.collection)

    def complete(self):
        return self.collection

    def collapse(self, span=None):
        complete = []
        #print(self.collection)
        start = 0
        end = len(self.collection)
        if span is not None:
            start = max(start, span[0])
            end = min(end, span[1])
        for ii in range(start, end):
            items = self.collection[str(ii)]
            if items:
                items = sorted(items, key=lambda x: x['conf'], reverse=True)
                complete.append(items[0])
            else:
                complete.append([])
        self.collapsed = complete
        return complete

    def stringify(self):
        if self.collapsed is None:
            self.collapse()
        string = ''
        for x in self.collapsed:
            if x:
                string += x['literal']
            else:
                string += ' '
        return string

    @property
    def extra(self):
        return self._extra

    @extra.setter
    def extra(self, extra):
        self._extra = extra

    @property
    def constraints(self):
        return self._constraints

    @constraints.setter
    def constraints(self, constraints):
        self._constraints = constraints

    def contains_selection(self):
        # extra = (new, select, flow)
        if self._extra is not None and len(self.extra) == 3:
            select = self._extra[1]
        else:
            print('LEVEL 0')
            return False
        if len(select) < 2:
            print('LEVEL 1')
            return False
        index = returnCEGindex(self._constraints, select)
        if index == -1:
            print('LEVEL 2')
            return False
        else:
            print('LEVEL 2')
            return True

    def halt(self):
        if self.halt:
            return False
        self.halt = True
        return True

    def name(self, string):
        self.title = string

    def parent(self, string):
        if self.title == '':
            return False
        if string == self.title:
            return False
        length = len(self.title)
        mark = string[0:length]
        if mark == self.title:
            return True
        else:
            return False

    def deeper(self, string):
        if len(self.title) == 0 or len(self.title) >= len(string):
            return True
        else:
            return False

    def __len__(self):
        return len(self.collection)

    @property
    def value(self):
        value = 0
        count = 0
        for x in self.collapse():
            if x:
                count += 1
                value += x['conf']
        if count != 0:
            return count, value / count
        else:
            return 0, 0

    def __or__(self, b):
        size_a, rating_a = self.value
        size_b, rating_b = b.value
        print('AGGR SIZES', size_a, size_b)
        print('AGGR RATING', rating_a, rating_b)
        difference = abs(size_a - size_b)
        print('AGGR DIFF', difference)

        loc_a, loc_b = self.contains_selection(), b.contains_selection()
        if loc_a and not loc_b:
            if rating_a > 80:
                print('BASED ON CONTENTS')
                print('SEL', 0)
                return self
        elif not loc_a and loc_b:
            if rating_b > 80:
                print('BASED ON CONTENTS')
                print('SEL', 1)
                return b

        #if size_a >= size_b:
        #    return b if rating_a*(difference+1) < rating_b else self
        #else:
        #    return self if rating_b*(difference+1) < rating_a else b
        coverage_a = size_a / len(self) if len(self) > 0 else 0
        coverage_b = size_b / len(b) if len(b) > 0 else 0
        if rating_a >= rating_b:
            if coverage_a >= coverage_b:
                print('SEL', 0)
                return self
            else:
                print('SEL', 1)
                return b
        else:
            if coverage_b >= coverage_a:
                print('SEL', 1)
                return b
            else:
                print('SEL', 0)
                return self


def damerau_levenshtein_distance(s1, s2):
    d = {}
    lenstr1 = len(s1)
    lenstr2 = len(s2)
    for i in range(-1, lenstr1 + 1):
        d[(i, -1)] = i + 1
    for j in range(-1, lenstr2 + 1):
        d[(-1, j)] = j + 1
    for i in range(lenstr1):
        for j in range(lenstr2):
            if s1[i] == s2[j]:
                cost = 0
            else:
                cost = 1
            #                   deletion,         insertion,             substitution
            d[(i, j)] = min(d[(i - 1, j)] + 1, d[(i, j - 1)] + 1, d[(i - 1, j - 1)] + cost)
            if i and j and s1[i] == s2[j - 1] and s1[i - 1] == s2[j]:
                d[(i, j)] = min(d[(i, j)], d[i - 2, j - 2] + cost) # transposition

    return d[lenstr1 - 1, lenstr2 - 1]


def transcribe(imagesize, elm, cont, hier, draw=False):

    import difflib
    import multiprocessing
    #import queue
    import uuid

    #class TextMatches(object):
    #    def __init__(self,seq):
    #        self.blocks = seq.get_matching_blocks()[:-1]

    #    def __len__(self):
    #        return len(self.blocks)

    #    def single(self):
    #        return self.blocks[0]

    def bestMatch(seq):
        offset = 0
        best = []
        blocks = seq.get_matching_blocks()
        #blocks = TextMatches(seq)
        if len(blocks) > 1:
            block = blocks[0]
            minimal = min(block.a, block.b)
            best.append([minimal, 0, 0])
            offset = block.a - block.b
        else:
            return []
        for block in blocks[:-1]:
            if block.a - block.b == offset:
                if block.size != 0:
                    best[-1][1] = min(block.a, block.b) + block.size - 1
                    best[-1][2] = best[-1][2] + block.size
            else:
                minimal = min(block.a, block.b)
                best.append([minimal, minimal + block.size, block.size])
        best = sorted(best, key=lambda x: x[2], reverse=True)
        best = [x for x in best if x[2] > 0]
        #print("BEST match",best)
        print('ALL', blocks)
        print('MATCH', best)
        return best

    def generate_line(line, draw=False):
        charspacing = max(max(line[0][1] - line[0][0], line[0][3] - line[0][2]) // 3, 1)
        img = drawCEGimage(imagesize, line, cont, hier, charspacing, 30, draw=draw)

        fileid = uuid.uuid4().hex
        filelocation = f'/tmp/{fileid}'
        cv2.imwrite(f'{filelocation}.png', cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
        data = call_tesseract([None], filename=filelocation)
        os.remove(f'{filelocation}.png')
        print(data)
        if not data:
            print(f'\x1b[1;31mNO TRANSCIPTIONS FROM TESS\x1b[0m')
            return None, None
        if len(data) < 2 or len(data[0]) == 0 or len(data[1]) == 0:
            print(f'\x1b[1;31mLESS THAN 2 TRANSCRIPTS FROM TESS\x1b[0m')
            return None, None
        print()
        local1 = "".join([x['literal'] for x in data[0]])
        local2 = "".join([x['literal'] for x in data[1]])
        seq = difflib.SequenceMatcher(a=local1.lower(), b=local2.lower())
        count = len(line)
        print('COUNT', count)
        print(local1, len(data[0]))
        print(local2, len(data[1]))
        return seq, data

    def initial_line_transcription(line, main, queue):
        import copy

        #print('LINE',line,main,queue)
        pos = 0
        s = 0
        e = len(line)
        original = copy.copy(line)
        while True:
            s_t = s
            e_t = e
            count = len(line)
            print('DRAW')
            seq, data = generate_line(line, draw=draw)
            if seq is None:
                print(f'\x1b[1;33mNO SEQUENCE FOUND\x1b[0m')
                r = None
            else:
                r = bestMatch(seq)
            if not r:
                print(f'\x1b[1;33mNO MATCHES FOUND\x1b[0m')
                s = s + 1
                e = e - 1
                #pos = pos + s
                line = original[s:e]
                if len(line) < 2:
                    print(f'\x1b[1;31m NO LINE LEFT TO ANALYZE\x1b[0m')
                    return False
                else:
                    continue
                #return False
            if len(data[0]) == len(data[1]) == count:
                if r[0][2] > 1:
                    aggr = queue.get()
                    aggr.insert(main + s, data[0])
                    aggr.insert(main + s, data[1])
                    queue.put(aggr)
                    return True
                else:
                    print(f'\x1b[1;31mSEQUENCE TOO SHORT\x1b[0m')
                    return False
            else:
                print('LENGTHS DIFFER')

                min_line_size = min(len(data[0]), len(data[1]))

                dist_l = abs(0 - r[0][0])
                dist_r = abs(min_line_size - r[0][1])

                print('DISTANCES FROM LINE TERMINALS', dist_l, dist_r)
                if dist_l <= dist_r:
                    s = s + r[0][0]
                    e = s + r[0][1] + 1
                    print('COMP L', s, e, s_t, e_t)
                    if s_t != s or e_t != e:
                        print('RETRY')
                        line = original[s:e]
                        #pos = pos + s_t
                        #s = s_t
                        #e = e_t
                        #print('POS L', pos)
                        #seq,data = generate_line(line)
                    else:
                        if dist_l == 0 and r[0][2] > 1:
                            print('MATCHES at 0')
                            aggr = queue.get()
                            aggr.insert(main + s, data[0][0:r[0][1] + 1])
                            aggr.insert(main + s, data[1][0:r[0][1] + 1])
                            queue.put(aggr)
                            return True
                        else:
                            #e = e - 1
                            s = s + 1
                            line = original[s:e]
                else:
                    #offset = len(line) - r[0][2]
                    s = len(line) - r[0][2]
                    e = s + r[0][2]
                    print('COMP R', s, e, s_t, e_t)
                    if s_t != s or e_t != e:
                        print('RETRY')
                        line = original[s:e]
                        #line = line[s_t:e_t]
                        #pos = pos + s_t
                        #s = s_t
                        #e = e_t
                        #print('POS R', pos)
                        #seq,data = generate_line(line)
                    else:
                        if dist_r == 0 and r[0][2] > 1:
                            print('MATCHES at -1')
                            aggr = queue.get()
                            aggr.insert(main + pos, data[0][len(data[0]) - r[0][2]:len(data[0])])
                            aggr.insert(main + pos, data[1][len(data[1]) - r[0][2]:len(data[1])])
                            queue.put(aggr)
                            return True
                        else:
                            e = e - 1
                            line = original[s:e]
                            #pos += 1
            if len(line) < 2:
                print(f'\x1b[1;31mNO LINE LEFT TO ANALYZE\x1b[0m')
                return False

    def empty_areas(collapse):
        areas = []
        idx = 0
        idy = 0
        trigger = False
        for i, e in enumerate(collapse):
            if e:
                if trigger:
                    idy = i
                    areas.append([idx, idy])
                    trigger = False
                    idx = i
                else:
                    idx = i
                print(i, e)
            else:
                trigger = True
                print(i)
        if trigger:
            areas.append([idx, i])
        print('EMPTY AREAS', areas)
        return areas

    def fill_in(i, e, empty, queue, elm, draw=False):
        l = e[0]
        r = e[1] + 1
        lim_l = empty[i - 1][1] if i != 0 else 0
        lim_r = empty[i + 1][0] if i != len(empty) - 1 else len(elm)
        print('LIMITS', lim_l, lim_r)
        trigger = False
        while r <= lim_r or l >= lim_l:
            print('#' * 16)
            if lim_r == r and l == lim_l:
                trigger = True
            print('SELECTION', l, r)
            seq, other = generate_line(elm[l:r], draw=draw)
            if other is None:
                print(f'\x1b[1;33mNO TRANSCRIPTS\x1b[0m')
                return False
            if seq is None:
                print(f'\x1b[1;33mNO SEQUENCE GENERATED\x1b[0m')
                return False

            #if len(other[0]) == len(other[1]) == 0:
            #    print(f'\x1b[1;33mEMPTY TRANSCRIPTS\x1b[0m')

            count = r - l
            start = l
            a = other[0]
            b = other[1]
            len_a = len(a)
            len_b = len(b)
            if len_a == len_b == count:
                print('BOTH EQUAL')
                match = bestMatch(seq)
                if match[0][2] > 1:
                    aggr = queue.get()
                    aggr.insert(start, other[0])
                    aggr.insert(start, other[1])
                    queue.put(aggr)
                    return True
                else:
                    print('NOT ENOUGH MATCHING')
            if (len_a == count or len_b == count) and abs(len_a - len_b) == 1:
                print('SOME EQUAL')
                main = None
                if len_a == count:
                    main = a
                else:
                    main = b

                blocks = seq.get_matching_blocks()[:-1]
                if len(blocks) == 1:
                    print('MISMATCH AT START OR END')
                    block = blocks[0]
                    if block.size == count:
                        print('X is a subset of Y')
                        aggr = queue.get()
                        aggr.insert(start, main)
                        queue.put(aggr)
                        return True
                    else:
                        success = False
                        aggr = queue.get()
                        aggr.collapse((l, r))
                        result = aggr.stringify()
                        searching_for = result.count(' ')
                        inserting = "".join([x['literal'] for x in main])
                        distance = damerau_levenshtein_distance(result, inserting)
                        print('RESULT', result, 'INSERTING', inserting, "MISSING", searching_for, "DISTANCE", distance)
                        if searching_for == distance:
                            aggr.insert(start, main)
                            success = True
                        queue.put(aggr)
                        if success:
                            return True
                elif a[0]['literal'] == b[0]['literal'] and a[-1]['literal'] == b[-1]['literal']:
                    print('MATCHING START AND END')
                    aggr = queue.get()
                    aggr.insert(start, main)
                    queue.put(aggr)
                    return True

            elif 3 < len_a == len_b == count + 1:
                pass
            else:
                print('NONE QUALIFY')

            l = max(lim_l, l - 1)
            r = min(lim_r, r + 1)

            if trigger:
                print(f'\x1b[1;33mRAN OUT OF CHOICES\x1b[0m')
                return False
        return False

    ######################################################################################################
    ######################################################################################################
    ######################################################################################################
    ######################################################################################################
    ######################################################################################################
    ######################################################################################################

    count = len(elm)
    aggr = Aggregator(count)

    lines = splitLines(elm)

    if len(lines) > 1:
        print(f'\x1b[1;33mINITIAL MULTILINE\x1b[0m')

        #seq, data = generate_line(elm, draw=draw)

        fileid = uuid.uuid4().hex
        charspacing = max(max(elm[0][1] - elm[0][0], elm[0][3] - elm[0][2]) // 3, 1)
        img = drawCEGimage(imagesize, elm, cont, hier, charspacing, 30, draw=draw)
        filelocation = f'/tmp/{fileid}'
        cv2.imwrite(f'{filelocation}.png', cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
        datapoints = call_tesseract([None], filename=filelocation)
        os.remove(f'{filelocation}.png')

        if len(datapoints) < 2:
            print(f'\x1b[1;33mLESS THAN 2 TRANSCRIPTIONS\x1b[0m')
        else:
            #print(count)
            #print()
            text1 = "".join([x['literal'] for x in datapoints[0]])
            text2 = "".join([x['literal'] for x in datapoints[1]])
            print('T1', text1, len(datapoints[0]))
            print('T2', text2, len(datapoints[1]))

            if len(datapoints[0]) == len(datapoints[1]) == count:
                seq = difflib.SequenceMatcher(a=text1.lower(), b=text2.lower())
                trigger = False
                for block in seq.get_matching_blocks():
                    if block.a != block.b and block.size > 1:
                        trigger = True
                        break
                if not trigger:
                    aggr.insert(0, datapoints[0])
                    aggr.insert(0, datapoints[1])
                    return aggr
    else:
        print(f'\x1b[1;33mNO MULTILINE\x1b[0m')

    queue = multiprocessing.Queue()
    queue.put(aggr)
    pos = 0

    threads = []
    for line in lines:
        t = multiprocessing.Process(target=initial_line_transcription, args=(line, pos, queue))
        pos += len(line)
        t.start()
        #t.join()
        threads.append(t)
    for t in threads:
        t.join()

    aggr = queue.get()
    collapse = aggr.collapse()
    string = aggr.stringify()
    start = 0
    trigger = True
    for i, l in enumerate(lines):
        present = string[start:start + len(l)].strip()
        print(f'Line {i}', present, len(present))
        start += len(l)
        if len(present) == 0:
            print(f'\x1b[1;31mLINE PROBLEM\x1b[0m')
        else:
            trigger = False

    if trigger:
        print(f'\x1b[1;31mNO LINES FOUND\x1b[0m')
        return None
    queue.put(aggr)

    empty = empty_areas(collapse)

    print(f'\n\n\x1b[1;32mFILLING...\x1b[0m\n')
    threads = []
    for i, e in enumerate(empty):
        t = multiprocessing.Process(target=fill_in, args=(i, e, empty, queue, elm), kwargs={'draw': draw})
        t.start()
        t.join()
        threads.append(t)
    for t in threads:
        t.join()

    aggr = queue.get()
    collapse = aggr.collapse()
    #empty = empty_areas(collapse)
    return aggr
#import time
#start_time = time.time()
#print("--- %s seconds ---" % (time.time() - start_time))
