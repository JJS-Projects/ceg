import numpy as np


class Node(object):
    global internal_grid

    def __init__(self, x, y, weight=1):
        if internal_grid[y][x] is None:
            self.x = x
            self.y = y
            self.g = 0
            self.f = 0
            self.h = 0
            self.w = weight
            self.opened = False
            self.closed = False
            self.parent = None
            internal_grid[y][x] = self
        else:
            copy = internal_grid[y][x]
            self.x = copy.x
            self.y = copy.y
            self.g = copy.g
            self.f = copy.f
            self.h = copy.h
            self.w = copy.w
            self.opened = copy.opened
            self.closed = copy.closed
            self.parent = copy.parent

    def __str__(self):
        return str({'x': self.x, 'y': self.y, 'f': f"{self.f:.2f}"})

    def __repr__(self):
        return str(self)


def gen_neighbours(x, y, grid):
    size_y, size_x = grid.shape[:2]
    neighbours = []
    if x != 0:
        if grid[y, x - 1] == 0:
            neighbours.append(Node(x - 1, y))
        else:
            neighbours.append(Node(x - 1, y, weight=max(size_y * 2, size_x * 2)))

    if x != size_x - 1:
        if grid[y, x + 1] == 0:
            neighbours.append(Node(x + 1, y))
        else:
            neighbours.append(Node(x + 1, y, weight=max(size_y * 2, size_x * 2)))

    if y != 0:
        if grid[y - 1, x] == 0:
            neighbours.append(Node(x, y - 1))
        else:
            neighbours.append(Node(x, y - 1, weight=max(size_y * 2, size_x * 2)))

    if y != size_y - 1:
        if grid[y + 1, x] == 0:
            neighbours.append(Node(x, y + 1))
        else:
            neighbours.append(Node(x, y + 1, weight=max(size_y * 2, size_x * 2)))

    return neighbours


def backtrace(node):
    nodes = []
    while node is not None:
        nodes.append((node.x, node.y))
        node = node.parent
    return nodes


internal_grid = None


def astar(startX, startY, endX, endY, grid):
    # initialize internal Node grid
    global internal_grid
    int_y, int_x = grid.shape[:2]
    internal_grid = []
    for i in range(0, int_y):
        internal_grid.append([None] * int_x)
    ###############################

    endNode = Node(endX, endY)
    startNode = Node(startX, startY)
    startNode.g = 0
    startNode.f = 0

    openlist = []
    openlist.append(startNode)
    startNode.opened = True

    while len(openlist) > 0:
        node = openlist.pop(0)
        node.closed = True

        if node.x == endNode.x and node.y == endNode.y:
            return backtrace(node)

        neighbours = gen_neighbours(node.x, node.y, grid)
        number = len(neighbours)
        for i in range(0, number):
            neighbor = neighbours[i]

            if neighbor.closed:
                openlist = sorted(openlist, key=lambda x: x.f)
                continue

            x = neighbor.x
            y = neighbor.y

            ng = node.g + neighbor.w

            if not neighbor.opened or ng < neighbor.g:
                neighbor.g = ng
                weight = neighbor.w
                neighbor.h = weight + np.linalg.norm(np.array([abs(x - endX), abs(y - endY)]))
                neighbor.f = neighbor.g + neighbor.h
                neighbor.parent = node

                if not neighbor.opened:
                    openlist.append(neighbor)
                    openlist = sorted(openlist, key=lambda x: x.f)
                    neighbor.opened = True
                else:
                    openlist = sorted(openlist, key=lambda x: x.f)
    return []


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import cv2

    size_x = 20
    size_y = 20
    start = [1, 18]

    plt.figure(figsize=(50, 20))

    # FIRST EXAMPLE
    np.random.seed(110)
    end = [7, 19]

    grid = np.reshape(np.where(np.random.rand(size_x * size_y) > 0.7, 1, 0) * 255, (size_y, size_x))
    orig = grid.copy()
    orig = np.uint8(orig)
    orig = cv2.cvtColor(orig, cv2.COLOR_GRAY2RGB)
    orig[start[1], start[0]] = [0, 255, 0]
    orig[end[1], end[0]] = [255, 0, 0]

    plt.subplot(221)
    plt.title('Grid Connected')
    plt.xticks(list(range(0, size_x)))
    plt.yticks(list(range(0, size_y)))
    plt.imshow(orig, cmap="gray")
    # 5.45 ms ± 52.9 µs per loop (mean ± std. dev. of 7 runs, 100 loops each) using sorted()
    # %timeit ans = astar(1,18,7,19,grid)
    ans = astar(start[0], start[1], end[0], end[1], grid)
    for elm in ans:
        grid[elm[1], elm[0]] = 127
    plt.subplot(222)
    plt.title('Best path')
    plt.xticks(list(range(0, size_x)))
    plt.yticks(list(range(0, size_y)))
    plt.imshow(grid, cmap="gray")

    # SECOND EXAMPLE
    np.random.seed(111)
    grid = np.reshape(np.where(np.random.rand(size_x * size_y) > 0.5, 1, 0) * 255, (size_y, size_x))
    end = [18, 1]
    orig = grid.copy()
    orig = np.uint8(orig)
    orig = cv2.cvtColor(orig, cv2.COLOR_GRAY2RGB)
    orig[start[1], start[0]] = [0, 255, 0]
    orig[end[1], end[0]] = [255, 0, 0]

    plt.subplot(223)
    plt.title('Grid Disconnected')
    plt.xticks(list(range(0, size_x)))
    plt.yticks(list(range(0, size_y)))
    plt.imshow(orig, cmap="gray")
    ans = astar(start[0], start[1], end[0], end[1], grid)
    for elm in ans:
        grid[elm[1], elm[0]] = 127
    plt.subplot(224)
    plt.title('Cheapest path')
    plt.xticks(list(range(0, size_x)))
    plt.yticks(list(range(0, size_y)))
    plt.imshow(grid, cmap="gray")

    plt.show()
